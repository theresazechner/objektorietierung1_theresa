import org.campus02.blackjack.BlackJack;
import org.campus02.blackjack.Player;

public class DemoBJ
{

	public static void main(String[] args)
	{
		Player p1 = new Player("Max Mustermann", 25);
		Player p2 = new Player("Susi Sorglos", 20);
		
		BlackJack bj = new BlackJack();
		
		bj.add(p1); // da ruf ich meine Methode von vorne auf und da sag ich player und Wert 0
		bj.add(p2);
		
		System.out.println("Default Werte");
		System.out.println(bj);
		
		bj.addCard(p1, 3);
		bj.addCard(p2, 10);
		System.out.println();
		
		System.out.println("HashMap Manipulation");
		System.out.println(bj);
		
		bj.addCard(p1, 14);
		bj.addCard(p2, 10);
		System.out.println();
		
		System.out.println("Finish ----");
		System.out.println(bj);
	}
}
