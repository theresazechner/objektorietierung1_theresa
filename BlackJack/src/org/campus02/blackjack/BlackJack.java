package org.campus02.blackjack;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

public class BlackJack
{
	private HashMap<Player, Integer> players = new HashMap<>(); // deklaration mit initialisierung
	
	public boolean add(Player player)
	{
		if(!players.containsKey(player)) // wenn er NICHT ist dann soll ich es hinzuf�gen!
		{
			players.put(player, 0);
			return true;
		}else
		{
			return false;	
		}
	}

	public boolean addCard(Player player, Integer cardValue)
	{
		if(players.containsKey(player))
		{
			int value = players.get(player);
			players.put(player, value + cardValue);
			return true;
		}
		return false;
		
		// mein Versuch
//		int card = 0;
//		if(players.containsValue(cardValue))
//		{
//			card = card + cardValue;
//			
//			return true;
//		}else
//		{
//			return false;
//		}
		
	}

	public int getValue(Player player)
	{
		return players.get(player); // man kann auch nur das sagen --> meines ist wenn ich es absichern m�chte das es wirklich so ist!
	}
		// mein Versuch
//		if(players.containsValue(player))
//		{
//			return players.get(player);
//		}
//		return 0;
//	}

	public String toString()
	{
		String text = "";
		Set<Entry<Player, Integer>> playersSet = players.entrySet();
		for (Entry<Player, Integer> entry : playersSet)
		{
			text += entry.getKey().getName() + ", " + entry.getValue() + "\n";
		}
		
		return text;
		
		// mein Versuch
//		return "players";
	}
	
}
