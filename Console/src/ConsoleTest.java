import java.util.Scanner;

public class ConsoleTest
{

	public static void main(String[] args)
	{
		System.out.println("Eingabe:");
		Scanner vonConsole = new Scanner(System.in);		
		
		int vonConsoleGelesen = vonConsole.nextInt();
		System.out.println(vonConsoleGelesen);
		
		System.out.println("geben Sie Ihren Namen ein:");
		
		String name = vonConsole.next();
		System.out.printf("Hallo '%s' du hast vorher %d eingegenen", name, vonConsoleGelesen);

	}

}
