import java.util.ArrayList;
import java.util.Collections;

public class DividerApp
{
	public static void main(String[] args)
	{
		greatestDivider(16);
		greatestDivider(71);
		greatestDivider(123);
		greatestDivider(1024);

		ArrayList<Integer> groeßterTeiler = new ArrayList<Integer>(); // <Integer> rufe ich die Klasse Interger auf

		for (int Teiler = 1; Teiler < 42; Teiler++)
		{
			int ergebnis = 42 % Teiler; // Modulo von 1 - 41 durch den Teiler

			if (ergebnis == 0) // alle Teiler bei denen das ergebnis 0 ist werden in der unteren Zeile
								// in groeßterTeiler gespeichert
			{
				groeßterTeiler.add(Teiler);
			}
		}

		Collections.max(groeßterTeiler); // der größter Teiler mit Modulo null wird herausgesucht
		System.out.println(Collections.max(groeßterTeiler)); // der größte Teiler wird heraus gegeben
	}

	public static int greatestDivider(int ganzzahl)
	{
		ArrayList<Integer> greatestDivider = new ArrayList<Integer>();

		int result = 0;
		for (int teiler = 1; teiler < ganzzahl; teiler++)
		{
			result = ganzzahl % teiler;

			if (result == 0)
			{
				greatestDivider.add(teiler);
			}
		}

		Collections.max(greatestDivider);
		System.out.println(Collections.max(greatestDivider));

		return result;
	}

}

/*
 * 1. Erstellen Sie ein einem Projekt (Divider) eine Klasse DividerApp mit einem
 * main Methode 2. Schreiben Sie ein Programm, das den größten Teiler ( Modulo %
 * ist in dem Fall 0) einer Zahl sucht. Suchen Sie zuerst in der main Methode
 * den Teiler der Zahl 42. 3. Erstellen Sie nun eine Methode greatestDivider die
 * nach dem selben Prinzip den Teiler einer Ganzzahl sucht. Die Ganzzahl wird
 * als Parameter übergeben und der größte Teiler als Ergebnis zurück gegeben. 4.
 * Rufen Sie diese greatestDivider in der main Methode für die Zahlen 16, 71,
 * 123 und 1024 auf und geben sie jeweils das Ergebnis auf der Konsole aus.
 */
