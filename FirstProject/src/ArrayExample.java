
public class ArrayExample
{

	public static void main(String[] args)
	{
		int [] feld1 = new int[] {5, 2, 4, 1, 3, 6, 7};
		
		 System.out.println(feld1[3]); 
		 
		 int stelle=4;
		 System.out.println(feld1[stelle]); // index aus einer variable
		
		 System.out.println(feld1.length); // Wert an der stelle 3
		 
		 printArray(feld1);
		 System.out.println();
		 
		 System.out.println();
			printArray(feld1); // ausgabe vor dem tasuchen
			swap(feld1, 6, 3); // tauschen
			printArray(feld1); // nach dem tasuchen
			
		 
		 ArrayMax.findMax(feld1); // verweis auf eine andere Klasse --> greift auf andere Methode zu, wenn public ist
		 
		 printArray(new int[]{1,2,3,4,5,6,7});
		
	}
	public static void printArray(int[] feld1)
	{
		 for(int index = 0; index < feld1.length; index ++) // alle inhalte aus dem Feld --> Array ausgeben
		 {
			 System.out.print(feld1[index] + " "); // on ln nebeneinander --> ln bedeuten in line also untereinander
		 }
		 System.out.println();
	}

	// Schreiben Sie eine Methode die in einem Feld zwei Stellen vertauscht
	// die Methode hat das Feld und die erste und zweite Stelle als Parameter
	public static void swap(int[] feld, int a, int b)
	{
			int temp = feld[a]; // wert an 1.Stelle merken
			feld[a] = feld[b]; // wert von 2. Stelle auf 1. Stelle schreiben
			feld[b] = temp; // gemerkter Wert an die 2.Stelle schreiben
	}

}
