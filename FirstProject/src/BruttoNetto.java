
public class BruttoNetto
{

	public static void main(String args[])
	{

		double netto = 30;
		double steuersatz = 1.2;
		double brutto = (netto * steuersatz);

		System.out.println(brutto + "Euro");

		System.out.printf("      netto: %.2f \n", netto); // %.2f  auf 2 Kommastellen runden \n Zeilenumbruch
		System.out.printf("      steuersatz: %.2f \n", steuersatz); // \n Zeilenumbruch
		System.out.printf("Bruttopreis: %.2f \n", brutto); // \n Zeilenumbruch
		
		int a = 1; // =ist zuweisung!
		
		if(a == 2) //if braucht immer einen boolean, == vergleich
		{
			System.out.println("a ist gleich 2");
		} else if (a == 3)
		{
			System.out.println("a ist nicht gleich2");
		}
		else
		{
			System.out.println("a ist nicht gleich 2 und 3");
		}
	}
}
