
public class ForEach
{

	public static void main(String[] args)
	{
		int[] zahlen = new int[] // [] bedeutet array
		{1,3,5,1,7,22,33,42};
		
		//wert= Name der variable : zahlen = feld das ich durchlaufen will
		for (int wert : zahlen) // die wird so oft durchlaufen wie das feld lang ist --> foreach Schleife = verk�rzte for schleife anstelle von  
								// --> for(int index = 0; index < feld1.length; index ++)
								// 		{
								//  		 int wert = feld1[index]; 
								// 		}
		{
			System.out.println(wert); // bei jedem durchlauf hat wert den zahlenwert an der stelle im feld
		}

		
		//Summe (--> variable) aller Zahlen berechnen
		
		
		int summe = 0;
		
		for (int zahlenwert : zahlen)
		{
			summe = summe + zahlenwert;
		}
		System.out.println(summe);
	}

}
