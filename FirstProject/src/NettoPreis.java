
public class NettoPreis
{

	public static void main(String[] args)
	{
		double netto = berechneNettoPreis(200, 2);
		System.out.println("huhu");
		System.out.println(netto);
		
		double brutto = BruttoNettoKategorie.bruttoPreis(200, 2);
		System.out.println(brutto);
	}

	
	// aus einem bruttoPreis einen nettoPreis berechnet
	
	public static double berechneNettoPreis(double bruttoPreis, int steuerKategorie)
	{
		double result;
		switch (steuerKategorie)
		{
		case 1:
			result = bruttoPreis / 1.2;
			break;
		case 2:
			result = bruttoPreis / 1.12;
			break;
		case 3:
			result = bruttoPreis / 1.1;
			break;
		default:
			result = bruttoPreis;
			break;
		}
		System.out.println(bruttoPreis + " " + steuerKategorie+ " " + result);
		return result;
	}
}
