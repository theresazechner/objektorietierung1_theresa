
public class WhileExample
{

	public static void main(String[] args)
	{
		double einlage = 2000;
		double zinssatz = 1.025;
		double zielbetrag = 3000;
		double kontostand = einlage;
		
		int jahre = 0;
		
		while(kontostand < zielbetrag)
		{
			jahre ++; // jahre = jahre + 1 --> ist das gleiche
			kontostand = kontostand * zinssatz;
			System.out.printf("%d: Jahre: %.2f \n", jahre, kontostand); // %.2f = auf 2 kommastellen runden --> \n = Zeilenumbruch
		}
		
		System.out.println(jahre + " Jahre \n");
	}

}
