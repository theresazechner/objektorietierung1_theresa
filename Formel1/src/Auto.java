
public class Auto
{ 
	private String farbe; // String weil farbe keine Zahlenkette ist --> es ist eine Zeichenkette
	private double tankstand; // blau weil es eine Membervariable ist --> gilt f�r der ganzen Klasse
	private double geschwindigkeit; // private hei�t es ist nicht �ffentlich sondern nur f�r diese Klasse zug�nglich
	
	public Auto(String autofarbe) // = Konstruktor --> hat keinen returnvalue --> wird aufgerufen wenn ich ein neues Auto mache
	{
		farbe = autofarbe;
	}
	
	public void volltanken()
	{
		tankstand = 100; // Tankstand wurde in der klasse deklariert --> darum kann ich es verweden
	}
	
	public void beschleunigen(double gas)
	{
		geschwindigkeit = geschwindigkeit + gas;
		if(geschwindigkeit > 350)
		{
			geschwindigkeit = 350;
		}
			checkStatus();
	}
	
	public void langsamer(double bremsen)
	{
		geschwindigkeit = geschwindigkeit - bremsen;
		if(geschwindigkeit <= 0)
		{
			geschwindigkeit = 0;
		}
			checkStatus();
	}

	public void fahren(double zeit) // sekunden
	{
		tankstand = tankstand - (geschwindigkeit * zeit)/ 70000;
		checkStatus();
	}
	
	private void checkStatus()
	{
		if(tankstand <= 0)
		{
			tankstand = 0;
			geschwindigkeit = 0;
		}
	}

	public void status()
	{
		System.out.println(farbe+ " " + geschwindigkeit + " " + tankstand); // leere "" hei�t dass es ein Leerzeichen ist
	}
	public String toString() // muss public sein
	{
		return farbe + " " + geschwindigkeit;
	}

}
