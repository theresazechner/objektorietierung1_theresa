
public class Monaco
{

	public static void main(String[] args)
	{
		Auto auto1 = new Auto("rot"); // new ist einen Referenzvariabel --> zeigt auf einen Speicherbereich  --> neues Objekt
		Auto auto2 = new Auto ("blau");
		Auto auto3 = new Auto ("gelb");
		Auto auto4 = auto1;
		
		System.out.println(auto1.toString()); // --> das brauch ich nicht schreiben es reicht die untere Variante weil ich das im Auto schon bei String toString geschrieben hab das ist gleich wie das untere
		System.out.println(auto1); // das ist gleich wie das obere
		
		auto1.status();
		
		auto1.volltanken();
		auto2.volltanken();
		auto3.volltanken();
		
		auto1.beschleunigen(10);
		auto1.status();
		
		auto1.fahren(120);
		auto1.status();
		
		test();
	}
	
	public static void test()
	{
		Auto auto1 = new Auto("gr�n"); // muss einen return wert angeben sonst wird das Auto "weggeschmissen", da es keinen verweis hat --> 
									  // ich fang damit so nichts an
	}

}
