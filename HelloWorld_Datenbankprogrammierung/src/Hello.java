import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;

public class Hello
{

	public static void main(String[] args)
	{
		
		MyDBHelper helper = new MyDBHelper();
		
		helper.init(); // Driver load, Con open
		
		System.out.println();
		
		helper.printAllUrlaube();
		
		System.out.println();
		
		helper.printAllStudents();
		
		System.out.println();
		
		helper.printAllUrlaubeWithPreisGreaterThanOrderByOrt(350);
		
		System.out.println();
		
		helper.printDetailsForStudentWithId(5);
	
		System.out.println();
		
		helper.printDetailsForStudentWithIdWithPreparedStatment(1);
		
		System.out.println();
		
		helper.printAllUrlaubeForStudentWithIdAndPriceGreaterThan(2,200);
		
		System.out.println();
		
		helper.printStudentAndUrlaube(1, 300);
		
		System.out.println();
		
		helper.printStudendAndUrlaube(1, 200);
		
		System.out.println();
		
		helper.printUrlaubsuebersichtForAllStudents();
		
		System.out.println();
		
		helper.printZusammenfassungProStudent();
		
		System.out.println();

		helper.printUrlaubsortStatistik();
		
		System.out.println();
		
		// mit affectedRows
		int result = helper.updateStudent1(2, 1500);
		
		if(result == 0)
		{
			System.out.println("Kein Student vom Update betroffen!");
		}else
		{
			System.out.println("%d Studierende betroffen: " + result);
		}
		
		System.out.println();
		
		//Ohne affected Rows
		helper.updateStudent(1, 1500);
		
		System.out.println();
		
		Student s = new Student("Max", "Mustermann", 1000, true, "blau", null);
		helper.addNewStudent(s);
		
		System.out.println();
		
		Student s1 = new Student("Susi", "Sauer", 1100, false, "gelb", null);
		int result2 = helper.addNewStudent1(s1);
		System.out.println(result2);
		
		System.out.println();
		
		helper.printAllStudents();
		
		helper.printColumnNamesForTableStudentFromMetadata();
		
		HashMap<String, String> resultMap = helper.getColumnNamesForTableStudentFromMetadata();
//		System.out.println(helper.getColumnNamesForTableStudentFromMetadata());
		
		//Lambda --> so kann ich auch eine HashMap in einer forEach() Schleife ausgeben
		resultMap.forEach((x,y)->System.out.printf("\n%-20s %-20s", x,y));
		System.out.println();
		
		System.out.println(helper.getStudentWithUrlaubsDetails());
		
		
		helper.close(); // Connection close
	}

}

//		System.out.println("Hello Campus02!");
//		try {;
//			// 1a. Jar-Files referenzieren - Java Build Path
//			//1b. Load Access Driver
//			Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
//		}
//		catch(ClassNotFoundException e) 
//		{
//			e.printStackTrace();
//		}
//		try {
//			
//			//2. get Connection to database
//			Connection con = DriverManager.getConnection("jdbc:ucanaccess://C:/Users/theresa.zechner/Downloads/Urlaubsverwaltung.accdb");
//			
//			System.out.println("Super - hat funktioniert!");
//			
//		} catch(SQLException e)
//		{
//			e.printStackTrace();
//		}