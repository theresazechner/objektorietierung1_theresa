import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

public class MyDBHelper {

	private Connection con = null;

	public void init() {

		try {
			// 1a. Jar-Files referenzieren - Java Build Path
			// 1b. Load Access Driver
			Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
		} catch (ClassNotFoundException e) {

			e.printStackTrace();
		}
		
		try {

			// 2. get Connection to database
			con = DriverManager.getConnection(
					"jdbc:ucanaccess://C:/Users/theresa.zechner/Documents/Datenbank/Urlaubsverwaltung.accdb");

			System.out.println("Super - hat funktioniert :-) ");

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public void close() {
		if (con != null)
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
				System.out.println("Fehler beim Schliessen der Verbindung");
			}
	}
	
	public void insertUrlaub(String destination, double preis)
	{
		//insert into MeineUrlaube(Ziel, price) .....
	}
	
	public void insertUrlaub(Urlaub urlaub)
	{
		//insert into MeineUrlaube(Ziel, price) .....
	}
	
	public void deleteUrlaub(int urlaubsNr)
	{
		
	}
	
	/*
	public ArrayList<String> GetAlleUrlaubPreisGroesserAls(double preis)
	{
		
		return null;
	}*/
	
	
	public void printAllUrlaube()
	{
		try 
		{
			Statement stmt = con.createStatement();
			String query = "SELECT Ort, Preis , Preis * 1.1 FROM Urlaubswunsch";
			ResultSet rs = stmt.executeQuery(query);
			
			while(rs.next()) // .next() wird der Cursor auf die erste Zeile gesetzt --> befindet sich zuerst bei -1
			{
				String ort = rs.getString(1); // 1 gezieht sich auf Ort
				double preis = rs.getDouble(2); // 2 bezieht sich auf Preis --> wenn ich es umdrehe steht dann steht oben 2 unten 1
												// bezieht sich nicht auf die Tabelle sondern auf welcher Stelle es in meinem SQL Statment steht
				double preisPlus10Prz = rs.getDouble(3);
				
				System.out.printf("Ort: %s Preis: %.2f \n", ort , preis);
			}
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	
	public void printAllStudents()
	{
		try
		{
			Statement stmt = con.createStatement();
			String query = "SELECT Vorname, Nachname, Credits, Inskribiert FROM Student";
			ResultSet rs = stmt.executeQuery(query);
			
			while(rs.next())
			{
				String vorname = rs.getString(1); // rs.get.. h�ngt von meinem Select Befehl zusammen
				String nachname = rs.getString(2);
				double credits = rs.getDouble(3);
				boolean inskribiert = rs.getBoolean(4);
				
				System.out.printf("Vorname: %s \t Nachname: %s \t Credits: %.2f \t Inskribiert: %s \n", vorname, nachname, credits, inskribiert);
			}
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	
	public void printAllUrlaubeWithPreisGreaterThanOrderByOrt(double preis)
	{
		try 
		{
			Statement stmt = con.createStatement();
			String query = "SELECT Ort, Preis FROM Urlaubswunsch WHERE Preis > " + preis +  " ORDER BY Ort asc";// Leerzeichen zwischen " " sonst steht es in einer "Wurst"
			ResultSet rs = stmt.executeQuery(query);
			
			while(rs.next())
			{
				String ort = rs.getString(1);
				double Preis = rs.getDouble(2);
				
				System.out.printf("Ort: %s Preis: %.2f \n", ort, Preis);
			}
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	
	public void printDetailsForStudentWithId(int id)
	{
		try 
		{
			Statement stmt = con.createStatement();
			String query = "SELECT Vorname, Nachname, StudentID, Inskribiert, Credits FROM Student WHERE StudentID = " + id;
			ResultSet rs = stmt.executeQuery(query);
			
			if(rs.next())
			{
				String vorname = rs.getString(1);
				String nachname = rs.getString(2);
				int studentid = rs.getInt(3);
				boolean inskribiert = rs.getBoolean(4);
				double credits = rs.getDouble(5);
				
				System.out.printf("Vorname: %s \t Nachname: %s \t StudentID: %d \t Inskribiert: %s \t Credits: %.2f \n", vorname, nachname, studentid, inskribiert, credits);
			}else
			{
				System.out.println("Student nicht gefunden!");
			}
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
		
	}
	
	
	public void printDetailsForStudentWithIdWithPreparedStatment(int studentid)
	{
		String query = "SELECT Vorname, Nachname, StudentID, Inskribiert, Credits FROM Student WHERE StudentID = ?";
		try 
		{
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setInt(1,studentid); // 1 bezieht sich aufs Fragezeichen im Statment
			ResultSet rs = stmt.executeQuery();
			
			if(rs.next())
			{
				String vorname = rs.getString(1);
				String nachname = rs.getString(2);
				int id = rs.getInt(3);
				boolean inskribiert = rs.getBoolean(4);
				double credits = rs.getDouble(5);
				
				System.out.printf("Vorname: %s \t Nachname: %s \t StudentID: %d \t Inskribiert: %s \t Credits: %.2f \n", vorname, nachname, id, inskribiert, credits);
			}else
			{
				System.out.println("Student nicht gefunden!");
			}
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	
	public void printAllUrlaubeForStudentWithIdAndPriceGreaterThan(int ID ,double price)
	{
		String query = "SELECT * FROM Urlaubswunsch WHERE StudentID = ? AND Preis > ?";
		try 
		{
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setInt(1,ID);// 1 bezieht sich aufs Fragezeichen im Statment
			stmt.setDouble(2,price);
			ResultSet rs = stmt.executeQuery();
			
			while(rs.next())
			{
				System.out.printf("StudentID: %d \t Ort: %s \t Preis: %.2f \n", rs.getInt(2), rs.getString(3), rs.getDouble(5));
			
			}
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	// meine Version --> ist das gleiche wie seine Version
	public void printStudentAndUrlaube(int ID ,double price)
	{
		String query = "SELECT Vorname FROM Student WHERE StudentID = ?";
		try 
		{
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setInt(1,ID);// 1 bezieht sich aufs Fragezeichen im Statment
			ResultSet rs = stmt.executeQuery();
			
			if(rs.next())
			{
				String vorname = rs.getString(1);
				System.out.printf("Student: %s \n", vorname);
				
				String queryUrlaube = "SELECT Preis, Ort FROM Urlaubswunsch WHERE Preis > ? AND StudentID = ?";
			
				PreparedStatement stmtFuerUrlaube = con.prepareStatement(queryUrlaube);
				stmtFuerUrlaube.setInt(2,ID);// 1 bezieht sich aufs Fragezeichen im Statment
				stmtFuerUrlaube.setDouble(1,price);
				ResultSet rsUrlaube = stmtFuerUrlaube.executeQuery();
				
				int counter= 0;
				while(rsUrlaube.next())
				{
					counter++;
					double preis = rsUrlaube.getDouble(1);
					String ort = rsUrlaube.getString(2);
			
					System.out.printf("Ort: %s \t Preis: %.2f \n", ort, preis);
				
				}if(counter == 0)
				{
					System.out.println("Wurde nicht gefunden!");
				}
			}else
			{
				System.out.println("Student nicht gefunden");
			}
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	// seine Version --> ist das gleiche wie meines
	public void printStudendAndUrlaube(int studentId, double preisGroesserAls) {
		String query = "SELECT Vorname FROM Student WHERE studentId=? ";
		try {
			PreparedStatement stmt = con.prepareStatement(query);		
			stmt.setInt(1, studentId);
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				String vorname = rs.getString(1);
				System.out.printf("Student: %s ", vorname);
				
				String queryUrlaube = "SELECT preis, Ort from Urlaubswunsch where preis>? and studentId=?";
			
				PreparedStatement stmtFuerUrlaube = con.prepareStatement(queryUrlaube);
				stmtFuerUrlaube.setDouble(1,preisGroesserAls);
				stmtFuerUrlaube.setInt(2, studentId);
				ResultSet rsUrlaube = stmtFuerUrlaube.executeQuery();
				int counter=0;
				while (rsUrlaube.next())
				{
					counter++;
					double preis = rsUrlaube.getDouble(1);
					String  ort= rsUrlaube.getString(2);				
			
					System.out.printf("Preis: %.2f Ort: %s \n", preis, ort);
				}
				if (counter==0)
				{
					System.out.println("Dieser Student hat keine Urlaubsw�nsche ");
				}
			}
			else
			{
				System.out.printf("Student: %d nicht gefunden ", studentId);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void printUrlaubsuebersichtForAllStudents()
	{
		String query = "SELECT * FROM Student";
		
		try {
			PreparedStatement stmt = con.prepareStatement(query);		
			ResultSet rs = stmt.executeQuery();
			
			while(rs.next())
			{
				// ist das gleiche wie wenn ich es gleich hinein chreib so wie unterhalb
				// 1.Variante
//				int studentId = rs.getInt(1);
//				printStudendAndUrlaube(studentId, 0);
				
				// 2.Variante
				printStudendAndUrlaube(rs.getInt(1), 0);
				System.out.println();
				
			}
		}catch(SQLException e)
		{
			e.printStackTrace();
		}	
	}
	
	
	public void printZusammenfassungProStudent()
	{
		String query = "SELECT Student.Vorname, avg(Urlaubswunsch.Preis), count(Urlaubswunsch.StudentID) "
						+ " FROM Student left join Urlaubswunsch ON Student.StudentID = Urlaubswunsch.StudentID"
						+ " GROUP BY Vorname"; 
						// left bezieht sich auf die Tabelle die ich zuerst eingebe
						// mit dem left join ... on... bekomm ich alle Daten von Studenten -->auch jene die keinen Urlaubswunsch haben sprich nicht verkn�pft sind
						// mit right join ... on.. bekomm ich alle Urlaubsw�nsche
						// FULL OUTER JOIN da bekomm ich alles von beiden Tabellen
						// INNER JOIN --> was sowohl links als auch rechts vorhanden ist
		try {
			PreparedStatement stmt = con.prepareStatement(query);		
			ResultSet rs = stmt.executeQuery();
			
			while(rs.next())
			{
				String vorname = rs.getString(1);
				double preis = rs.getDouble(2);
				int studentenId = rs.getInt(3);
				
				System.out.printf("Vorname: %s \t Preis: %.2f \t Anzahl: %d \n", vorname, preis, studentenId);
			}
		}
			catch(SQLException e)
			{
				e.printStackTrace();
			}
	}
	
	public void printUrlaubsortStatistik()
	{
		String query = "SELECT Ort, COUNT(Ort) FROM Urlaubswunsch GROUP BY Ort"
				+ " HAVING COUNT(*) > 0" // gib nur die aus die gr��er 0 sind!
				+ " ORDER BY COUNT(*) desc"; // oder COUNT(*) --> ohne * funktioniert
				//es wenn der Wert nicht NULL ist sonst hab ich ohne Stern ein Problem wenn ich alles ausgeben m�chte!!
		try {
			PreparedStatement stmt = con.prepareStatement(query);		
			ResultSet rs = stmt.executeQuery();
			
			while(rs.next())
			{
				String ort = rs.getString(1);
				int anzahl = rs.getInt(2);
				
				System.out.printf("Ort: %s Anzahl: %d \n", ort, anzahl);
			}
		}
			catch(SQLException e)
			{
				e.printStackTrace();
			}
	}
	
	// seine Methode mit return Wert
	public int updateStudent1(int studentID, double newCredits)
	{
		int affectedRows = 0;
		// 1a - update Statement
		String query = "UPDATE Student SET Credits= ? WHERE StudentID = ?";
		try {
			
			//1b - Prepared Statement
			PreparedStatement stmt = con.prepareStatement(query);
			
			// 1c - Parameter setzen
			stmt.setInt(1, studentID);
			stmt.setDouble(2, newCredits);
			
			affectedRows = stmt.executeUpdate();
		}
			catch(SQLException e)
			{
				e.printStackTrace();
			}
		return affectedRows;
	}
	
	// meine Methode ohne return-Wert
	public void updateStudent(int studentID, double newCredits)
	{
		String query = "UPDATE Student SET Credits= ? WHERE StudentID = ?";
		try {
			PreparedStatement stmt = con.prepareStatement(query);
			
			stmt.setInt(1, 1500);
			stmt.setDouble(2, 1);
			stmt.executeUpdate();
		}
			catch(SQLException e)
			{
				e.printStackTrace();
			}
	}
	
	// mit R�ckgabewert
	public int addNewStudent1(Student s)
	{
		int affectedRows = 0;
		// 1a - update Statement
		String query = "INSERT INTO Student(Vorname, Credits) VALUES(?,?)";
		int newIdentity = 0;
		try {
			
			//1b - Prepared Statement
			PreparedStatement stmt = con.prepareStatement(query);
			
			// 1c - Parameter setzen
			stmt.setString(1, s.getVorname());
			stmt.setDouble(2, s.getCredits());
			
			affectedRows = stmt.executeUpdate();
			
			String newIdentityString = "SELECT @@identity";
			
			Statement stmt1 = con.createStatement();
			ResultSet rs = stmt1.executeQuery(newIdentityString);
			
			rs.next();
			
			newIdentity = rs.getInt(1);
		}
			catch(SQLException e)
			{
				e.printStackTrace();
			}
		return newIdentity;
	}
	
	// ohne R�ckgabewert --> bei Pr�fung sollen wir es dann mit R�ckgabewert machen
	public void addNewStudent(Student s)
	{
		String query = "INSERT INTO Student(Vorname, Nachname, Credits, Inskribiert, Lieblingsfarbe) "
						+ " VALUES(?,?,?,?,?)";
		
		
		try {
			PreparedStatement stmt = con.prepareStatement(query);
			
			stmt.setObject(1, s.getVorname());
			stmt.setObject(2, s.getNachname());
			stmt.setObject(3, s.getCredits());
			stmt.setObject(4, s.isInskribiert());
			stmt.setObject(5, s.getLieblingsfarbe());
			
			stmt.executeUpdate();
			
			String id = "SELECT last(StudentID) FROM Student";
			
			PreparedStatement stmt1 = con.prepareStatement(id);
			ResultSet rs = stmt1.executeQuery();
			
			while(rs.next())
			{
				System.out.printf("StudentID: ", rs.getInt(1));
			}
		}
			catch(SQLException e)
			{
				e.printStackTrace();
			}
	}
	
	// Stoff lesen in Java ist mehr als eine Insel Kapitel 16.11 Metadata
	public void printColumnNamesForTableStudentFromMetadata()
	{ //Vorname, Nachname,...
		
		try {		
			ResultSet rs = con.createStatement().executeQuery("SELECT * FROM Student");
			ResultSetMetaData meta = rs.getMetaData();
			
			
			// das gleiche wie ein createStatement nur mit einem prepareStatement
//			ResultSet rs = con.prepareStatement("SELECT * FROM Student").executeQuery();
//			ResultSetMetaData meta = rs.getMetaData();
			
		      for ( int i = 1; i <= meta.getColumnCount(); i++ )
		      {
		    	  // - = linksb�ndig ohne minus ist es rechtsb�ndig und 20 hei�t 20x Abstand
		        System.out.printf("\n\t %-20s %-20s%n", meta.getColumnName(i), meta.getColumnTypeName(i));
		      }
		}
			catch(SQLException e)
			{
				e.printStackTrace();
			}
	}
	
	public HashMap<String,String> getColumnNamesForTableStudentFromMetadata()
	{
		HashMap<String, String> map = new HashMap<>();
		
		
		try {		
			ResultSet rs = con.createStatement().executeQuery("SELECT * FROM Student");
			ResultSetMetaData meta = rs.getMetaData();
			
			 for ( int i = 1; i <= meta.getColumnCount(); i++ )
		      {
		    	 // k�nnte ich auch weglassen
//		        System.out.printf("\n\t %-20s %-20s%n", meta.getColumnName(i), meta.getColumnTypeName(i));
		        map.put(meta.getColumnName(i), meta.getColumnTypeName(i));
		      }
		}
			catch(SQLException e)
			{
				e.printStackTrace();
			}
		return map;
	}
	
	// !!!!!!NOCH NICHT FERTIG!!!!!!! DAS NICHT FERTIG ABGESCHRIEBENE VON IHM!!
	public Student getStudentWithUrlaubsDetails(int studentID)
	{
//		Student s = new Student();
//		s.setVorname("Josefa");
//		s.setCredits(250);
		
		ArrayList<Urlaub> urlaubswuensche = new ArrayList<>();
		Urlaub u1 = new Urlaub();
		Urlaub u2 = new Urlaub();
		
		urlaubswuensche.add(u1);
		urlaubswuensche.add(u2);
		String query = "SELECT StudentID, Vorname, Nachname FROM Student";
		
		try {
		
			PreparedStatement stmt = con.prepareStatement(query);
			ResultSet rs = stmt.executeQuery();
			
			String query1 = "SELECT Ort, Reihung, Preis FROM Urlaubswunsch";
			PreparedStatement stmt1 = con.prepareStatement(query1);
			ResultSet rs1 = stmt1.executeQuery();
			
			
			while(rs.next())
			{
				
				
			}
			while(rs1.next())
			{
				
			}
		}
			catch(SQLException e)
			{
				e.printStackTrace();
			}
		return null;
		
		
	}
	
	// !!!!!!NOCH NICHT FERTIG!!!!! --> ist das gleiche wie oben!!! MEIN VERSUCH!!
	public Student getStudentWithUrlaubsDetails()
	{
		Student s = null;
		Urlaub u = null;
		String query = "SELECT StudentID, Vorname, Nachname FROM Student";
		
		try {
		
			PreparedStatement stmt = con.prepareStatement(query);
			ResultSet rs = stmt.executeQuery();
			
			String query1 = "SELECT Ort, Reihung, Preis FROM Urlaubswunsch";
			PreparedStatement stmt1 = con.prepareStatement(query1);
			ResultSet rs1 = stmt1.executeQuery();
			
			
			rs.next();
			
			System.out.printf("%d %s %s",rs.getInt(1), rs.getString(2), rs.getString(3));
//				s= new Student(rs.getInt(1), rs.getString(2), rs.getString(3));
//				System.out.println(s);
				
			
			while(rs1.next())
			{
				System.out.printf("%s %s %.2f",rs1.getString(1), rs1.getBoolean(2), rs1.getDouble(3));
//				u = rs1.getString(1), rs1.getBoolean(2), rs1.getDouble(3);
			}
		}
			catch(SQLException e)
			{
				e.printStackTrace();
			}
		return s;
		
		
	}
	
	public ArrayList<Urlaub> GetAlleUrlaubPreisGroesserAls(double preis)
	{
		return null;
	}


	public void listKunden() {

		try {

			Statement stmt = con.createStatement();
			String sql = "SELECT * FROM Kunde ";

			ResultSet rs = stmt.executeQuery(sql);

			System.out.println("Meine Kunden");
			while (rs.next()) {
				System.out.print("Kdnr: " + rs.getInt(1));
				System.out.println(" Vorname " + rs.getString(2));

			}
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
}