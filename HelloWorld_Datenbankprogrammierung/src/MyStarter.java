import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MyStarter {

	public static void main(String[] args)  {
		
		System.out.println("Hello Campus02");
		
		
		try {
			Class.forName("MyStarter");
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		 try {
			DriverManager.getConnection("MyStarter");
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		//try 
			{
			try {
				demoEins();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} 
	//catch (Exception e) {
		//	System.out.println("Stack Trace main");
			//e.printStackTrace();
		//	System.out.println("oje - ein Fehler");
		//}

	}

	
	public static void demoEins() throws Exception //Zwang - der Aufrufer MUSS ein Try Catch verwenden
	{
		System.out.println("Hello World aus Demo eins");
		
		int i=10, j;
		
		j= 0;
		
		int erg = 0;
		
		try
		{
			erg= i / j; //Kein Catch deshalb weiter hochwerfen throw
		}
		catch (Exception e)
		{
			System.out.println("Stack Trace demo eins");
			e.printStackTrace();
			throw e; 
		}
		
		System.out.println(erg);
	}
}