import java.util.ArrayList;

public class Student {

	private String vorname;
	private String nachname;
	private double credits;
	private boolean inskribiert;
	private String lieblingsfarbe;
	private ArrayList<Urlaub> urlaubswuensche;
	
	public Student(String vorname, String nachname, double credits, boolean inskribiert, String lieblingsfarbe,
					ArrayList<Urlaub> urlaubswuensche) {
		super();
		this.vorname = vorname;
		this.nachname = nachname;
		this.credits = credits;
		this.inskribiert = inskribiert;
		this.lieblingsfarbe = lieblingsfarbe;
		this.urlaubswuensche = urlaubswuensche;
	}

	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	public String getNachname() {
		return nachname;
	}

	public void setNachname(String nachname) {
		this.nachname = nachname;
	}

	public double getCredits() {
		return credits;
	}

	public void setCredits(double credits) {
		this.credits = credits;
	}

	public boolean isInskribiert() {
		return inskribiert;
	}

	public void setInskribiert(boolean inskribiert) {
		this.inskribiert = inskribiert;
	}

	public String getLieblingsfarbe() {
		return lieblingsfarbe;
	}

	public void setLieblingsfarbe(String lieblingsfarbe) {
		this.lieblingsfarbe = lieblingsfarbe;
	}

	public ArrayList<Urlaub> getUrlaubswuensche() {
		return urlaubswuensche;
	}

	public void setUrlaubswuensche(ArrayList<Urlaub> urlaubswuensche) {
		this.urlaubswuensche = urlaubswuensche;
	}
	
	
}
