
public class Urlaub
{

	private String ort;
	private int reihung;
	private double preis;

	public String getOrt() {
		return ort;
	}

	public void setOrt(String ort) {
		this.ort = ort;
	}

	public int getReihung() {
		return reihung;
	}

	public void setReihung(int reihung) {
		this.reihung = reihung;
	}

	public double getPreis() {
		return preis;
	}

	public void setPreis(double preis) {
		this.preis = preis;
	}
	
	
}
