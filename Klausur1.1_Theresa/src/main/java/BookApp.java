import at.campus02.ase.books.Bibliothek;
import at.campus02.ase.books.Book;

public class BookApp {

	public static void main(String[] args) 
	{
		Bibliothek bibliothek = new Bibliothek();
		
		
		Book buch1 = new Book("auto1","Die Wahrheit",300);
		Book buch2 = new Book("autor2","Blutsbr�der",50);
		Book buch3 = new Book("autor3","Tats�chlich",200);
		
		bibliothek.addBook(buch1);
		bibliothek.addBook(buch2);
		bibliothek.addBook(buch3);
		
		System.out.println(buch1.getSeiten());
		System.out.println(buch2.getSeiten());
		System.out.println(buch3.getSeiten());
		
		System.out.println(buch1.getSeiten() + buch2.getSeiten() + buch3.getSeiten());
		
		System.out.println(bibliothek.countPages()); // so ist die einfachste methode und in der L�sung die richtige
	}

}
