package at.campus02.ase.books;

import java.util.ArrayList;

public class Bibliothek {

	private ArrayList<Book> buecherliste = new ArrayList<Book>();
	
	public void addBook(Book b) 
	{
		buecherliste.add(b);
	}

	public int countPages() 
	{
		int result = 0;
		
		for(int index = 0; index < buecherliste.size(); index++)
		{
			Book seiten = buecherliste.get(index);
			
			result = result + seiten.getSeiten();
		}
		
		return result;
	}

	public double avaragePages() 
	{
		double result = 0;
		
		double durchschnitt = countPages();
		
			result = result + durchschnitt / buecherliste.size();
		
		return result;
	}

	public ArrayList<Book> booksByAuthor(String autor) 
	{
		ArrayList<Book> gefundenes_buch_vom_autor = new ArrayList<Book>();
		
		if(autor == null) // wenn nichts drinnen ist braucht er garnicht in die Methode gehen!
		{
			return gefundenes_buch_vom_autor;
		}
		
		Book finde_file_mit_autor = buecherliste.get(0);
		
		for(int index = 0; index < buecherliste.size(); index++)
		{
			finde_file_mit_autor = buecherliste.get(index);
//			System.out.println(autor);
//			System.out.println(finde_file_mit_autor.getAutor());
			if(finde_file_mit_autor.getAutor() == autor)
			{
				gefundenes_buch_vom_autor.add(finde_file_mit_autor);
			}
		}
		if(gefundenes_buch_vom_autor.size() == 0)
		{
		return null;
		}else
		{
			return gefundenes_buch_vom_autor;
		}
	}

	public ArrayList<Book> findBook(String search) 
	{
		ArrayList<Book> uebereinstimmung_buecher = new ArrayList<Book>();
		
		if(search == null)
		{
			return uebereinstimmung_buecher;
		}
		
		Book finde_match_methode = buecherliste.get(0);

		for(int index = 0; index < buecherliste.size(); index++)
		{
			finde_match_methode = buecherliste.get(index);
			
			if(finde_match_methode.match(search) == true) // == true m�sste ich nicht machen da die Methode in der Klasse Book true ist
			{
				uebereinstimmung_buecher.add(finde_match_methode);
			}
		}
		
		return uebereinstimmung_buecher;
	}

}
