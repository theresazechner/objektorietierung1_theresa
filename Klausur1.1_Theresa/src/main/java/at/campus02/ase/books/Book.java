package at.campus02.ase.books;

public class Book {
	
	private String autor;
	private String titel;
	private int seiten;

	public Book(String autor, String titel, int seiten) {
		
		this.autor = autor;
		this.titel = titel;
		this.seiten = seiten;
	}
	

	public String getAutor() {
		return autor;
	}

	public String getTitel() {
		return titel;
	}

	public int getSeiten() {
		return seiten;
	}

	public boolean match(String search) 
	{
		String finde_titel = getTitel();
		String finde_autor = getAutor();
		System.out.println(finde_titel);
		System.out.println(finde_autor);
		System.out.println(search);
		
		if(finde_titel.contains(search)  || finde_autor.contains(search))
		{
			return true;
		}else
		{
		return false;
		}
	}

	public String toString()
	{
		return "[" + autor + "," + titel + "," + seiten + "]";
	}
}
