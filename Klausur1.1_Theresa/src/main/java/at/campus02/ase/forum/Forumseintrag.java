package at.campus02.ase.forum;

import java.util.ArrayList;

public class Forumseintrag {
	
	private String titel;
	private String text;
	
	private ArrayList<Forumseintrag> forum = new ArrayList<Forumseintrag>();

	public Forumseintrag(String titel, String text) {
		
		this.titel = titel;
		this.text = text;
		
	}

	public Forumseintrag antworten(String titel, String text) 
	{
		Forumseintrag result = new Forumseintrag(titel,text);
		
		forum.add(result);
		
		return result;
	}

	public ArrayList<Forumseintrag> getAntworten() 
	{
		return forum;
	}

	public int anzahlDerEintraege() 
	{
		int result = 1; // ich starte mit dem Forum das z�hlt schon als 1 eintrag!
		
		Forumseintrag eintraege = forum.get(0);
		ArrayList<Forumseintrag> antworten_vom_eintrag = eintraege.getAntworten();
		
		for(int index = 0; index < forum.size(); index++)
		{
			eintraege = forum.get(index);
			antworten_vom_eintrag = eintraege.getAntworten();
			result = result + antworten_vom_eintrag.size() + 1;
			System.out.println(result);
			
//			result = result + forum.get(index).anzahlDerEintraege();
//			System.out.println(result);
		}
		
		return result;
	}

	public String toString()
	{
		String antworten = "";
		
		for (Forumseintrag forumseintrag : forum) 
		{
			antworten = antworten + forumseintrag.toString();
		}
			
		if(!antworten.isEmpty())
		{
			antworten = "[" + antworten + "]";
		}
		
//		return "(" + titel + "," + text + ")"; --> vom Konstruktortest --> brauch ich jetzt nicht mehr --> aufgabe konstruktor richtig
		
		return String.format("(%s,%s)%s",titel ,text, antworten);
	}
}
