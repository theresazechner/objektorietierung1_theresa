import at.campus02.ase.books.Bibliothek;
import at.campus02.ase.books.Book;

public class BookApp
{
   public static void main(String [] args)
   {
	   Bibliothek bibliothek1 = new Bibliothek(); // Objekt Bibliothek in meienr App erstellen
	   
	   Book buch1 = new Book("Autor1", "Hallo", 200);  // Objekt Book erstellen
	   Book buch2 = new Book("Autor2", "Hansi", 150);
	   Book buch3 = new Book("Autor3", "Susi" , 300);
	   
	   bibliothek1.addBook(buch1); // B�cher den Objekt Bibliothek hinzuf�gen
	   bibliothek1.addBook(buch2);
	   bibliothek1.addBook(buch3);
	   
	   bibliothek1.countPages(); // Methodenaufruf countPages um die Seiten der B�cher zu z�hlen und zu summieren
	   
	   System.out.println(bibliothek1.countPages()); // Ausgabe der summierten Buchseiten
	    
   }

	
}
