package at.campus02.ase.books;

import java.util.ArrayList;

public class Bibliothek
{

	private ArrayList<Book> bibliothek = new ArrayList<Book>(); // = Membervariable

	public void addBook(Book b) // Book ist der Dateityp und b ist der Name --> k�nnte genau so hansi oder irgendwie anders hei�en
	{

		bibliothek.add(b); // .add ist eine Funktion von AarrayList um Eintrag hinzuzuf�gen--> Book kommt aus der Klasse Book 
						   // in dem Test sind schon beispielb�cher definiert und wir greifen darauf zur�ck ohne selber welche zu erzeugen

	}

	public int countPages()
	{
		int result = 0;

		for (Book buch : bibliothek)
		{
			result = result + buch.getSeiten();

		}

		return result;
	}

	public double avaragePages() // schnelle L�sung: return (double) countPages() / bibliothek.size();
								 // --> Aufruf der countPage() Methode + weiterer Rechenschritt f�r Durschnitt
	{
//		return (double) countPages() / bibliothek.size(); --> schnellere Variante, ich rufe in der averagePage die countPages Methode auf
		
		double result = 0;
		
		for (Book buch : bibliothek)
		{
			result = result + buch.getSeiten() / (double)bibliothek.size(); // Summe der Seiten / AttayList.size (durch die Anzahl der B�cher)
																			// Array befehl .size berechnet den Durchschnitt
																			// (double) deswegen weil das ergebnis ein double ist
																			// und aber .getSeiten und .size ein int ist darum muss man durch double dividieren
																			// unten erspare ich mir double weil ich double durch int rechne
		}
		
		return result;  // (double) = "interpretiere" int als double damit berechnung lt unit test
						// stimmt =)
						// return result / bibliothek.size(); --> man kann entweder so wie oben (double) hinzuf�gen
					   // also alles in einem oder zuerst alles zusammenz�hlen und erst unten dividieren		
	}

	public ArrayList<Book> booksByAuthor(String autor)
	{
		ArrayList<Book> autoren = new ArrayList<Book>(); // zum abspeichern f�r die herausgesuchten Autoren
		
		for (Book buch : bibliothek) // durchsuchen ArrayList
		{
			if(autor == buch.getAutor()) // wenn autor = gesuchter Autor...
			{
				autoren.add(buch); // f�ge zu ArrayList autoren hinzu
			}
			if(autoren == null) // wenn autoren = keine Eingabe..
			{
				return null; // ... gib 0 aus
			}
		}
		
		return autoren; // gibt ArrayList autoren zur�ck
	}

	public ArrayList<Book> findBook(String search)
	{
		
		ArrayList<Book> findeBuch = new ArrayList<Book>();
		
		for (Book buch : bibliothek)
		{
			if(search == null) // wenn die Suche keien String enth�lt
							  // er schaut zuerst ob etwas drinnen ist und dann gfeht er noch einmal die Methode durch --> wenn ich es umdrehe dann w�rde er nicht noch einmal in die Schleife gehen
			{
				return findeBuch; // gib die l�ere ArrayList searchAutor zur�ck
			}
			
			if(buch.match(search) == true) // wenn Methodenaufruf match = true
			{
				findeBuch.add(buch); // f�ge das Buch der ArrayList searchAutor hinzu
			}
			
		}
		return findeBuch; // gib ArrayList searchAutor zur�ck

	}	
}
