package at.campus02.ase.books;

public class Book {

	private String autor;
	private String titel;
	private int seiten;
	
	
	public Book(String autor, String titel, int seiten)
	{
		this.autor = autor;
		this.titel = titel;
		this.seiten = seiten;
		
	}

	public String getAutor() {
		return autor; // keine "" weil kein String sondern ein Objekt
	}

	public String getTitel() {
		return titel; // keine "" weil kein String sondern ein Objekt
	}

	public int getSeiten() 
	{
		return seiten;
	}

	public boolean match(String search)
	{
//		String str1 = autor; // ich hab meine Variablen oben schon deklariert
//		String str2 = titel;
		
//		str1.contains(autor);
//		str2.contains(titel);
		
		if(autor.contains(search) || titel.contains(search)) // wenn titel oder autor contains string search
															 // --> wenn die eingegebenen Zeichenketten enthalten sind
		{
			return true; // true 
		}
		else
		{		
		return false; // sonst false
		}
	}

	public String toString()
	{
		return "[" + autor + "," + titel + "," + seiten + "]"; // autor nehme ich von oben darum nicht unter Leerzeichen
															   // und [] und , unter "" weil ich das ausgeben m�chte
	}

}
