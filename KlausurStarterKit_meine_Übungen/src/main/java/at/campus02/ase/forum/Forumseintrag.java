package at.campus02.ase.forum;

import java.util.ArrayList;

public class Forumseintrag {

	private String titel;
	private String text;
	
	public ArrayList<Forumseintrag> antwort = new ArrayList<Forumseintrag>();
	
	public Forumseintrag(String titel, String text) 
	{
		this.titel = titel;
		this.text = text;
	}

	public Forumseintrag antworten(String titel, String text)
	{
		Forumseintrag forum1 = new Forumseintrag(titel, text); // neuer Forumseintrag --> neues Objekt
		
		antwort.add(forum1); // fourum1 wird meiner ArraxList hinzugef�gt
		
		
		return forum1; // ich gebe diesen Eintrag aus
		
	}

	public ArrayList<Forumseintrag> getAntworten() 
	{
		return antwort;
	}

	
	public int anzahlDerEintraege() 
	{
		int result = 0;
		for (Forumseintrag eintrag : antwort)
		{
			result = result + eintrag.antwort.size() + 1;  // Rekursion --> zeig mir alle Antworten zu den einzelnen Eintr�gen und summiere sie
															// +1 weil die "antwort"(Haupteintrag) auch noch mitgerechnet wird und nicht nur die Eintr�ge
														  // ich hab f�r jeden Forumseintrag eine eigene Liste
		}
		
		return result;
	}

	public String toString()
	{
		return "(" + titel + "," + text + ")";
	}
}
