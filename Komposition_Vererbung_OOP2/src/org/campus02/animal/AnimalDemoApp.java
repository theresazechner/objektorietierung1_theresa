package org.campus02.animal;

public class AnimalDemoApp
{

	public static void main(String [] args)
	{
		Dog dog1 = new Dog();
		dog1.name = "Peppi";
		dog1.makeNoise();
		dog1.move();
		
		Cat cat1 = new Cat();
		cat1.name = "Mietzi";
		cat1.makeNoise();
		cat1.move();
		
		Beagle beagle = new Beagle();
		beagle.loveFood = "Knochen"; // ich sag damit er mag Knochen
		beagle.name = "Sissy";
		beagle.makeNoise();
	}
}
