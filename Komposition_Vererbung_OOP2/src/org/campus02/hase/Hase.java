package org.campus02.hase;

public class Hase
{
	protected String name; // muss protected sein
	
	
	public Hase(String name)
	{
		this.name = name;
	}

	public void schlafen()
	{
		System.out.println(name + " schl�ft");
	}

	public void hoppeln()
	{
		System.out.println(name + " hoppelt");
	}
	
	public void fressen()
	{
		System.out.println(name + " frisst");
	}
}
