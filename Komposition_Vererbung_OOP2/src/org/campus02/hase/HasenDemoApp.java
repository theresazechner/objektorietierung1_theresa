package org.campus02.hase;

public class HasenDemoApp
{

	public static void main(String[] args)
	{
		Hasenstall stall1 = new Hasenstall();
		Hase hase1 = new Hase("Hoppel der Hase");
		Weihnachtshase w_hase1 = new Weihnachtshase("Flocke der Weihnachtshase"); 
		Osterhase o_hase1 = new Osterhase("Stups der kleine Osterhase");
		
		hase1.fressen();
		hase1.hoppeln();
		hase1.schlafen();
		
		System.out.println();
		
		w_hase1.fressen();
		w_hase1.geschenke_verteilen();
		w_hase1.hoppeln();
		w_hase1.schlafen();
		
		System.out.println();
		
		o_hase1.fressen();
		o_hase1.hoppeln();
		o_hase1.ostereier_verstecken();
		o_hase1.schlafen();

		System.out.println();
		
		// ist da gleiche wie die untere add Funktion 
//		stall1.hasenstall.add(o_hase1);
//		stall1.hasenstall.add(w_hase1);
		
		// ich hab bei Hasenstall eine add Funktion erstellt --> jetzt kann ich direkt auf diese zugreifen
		stall1.addHase(o_hase1);
		stall1.addHase(w_hase1);
		
		// Ausgabe von der toString Methode
		System.out.println(stall1);
		
		stall1.fressenHasen();
		
		System.out.println();
		
		stall1.schlafenHasen();
		
		System.out.println();
		
		// nicht von der Klasse
		System.out.println("up-casting");
		Hase wh2 = new Weihnachtshase("Peppi");
		Hase oh2 = new Osterhase("Fritzi");
		oh2.fressen();
		wh2.schlafen();
		
		System.out.println();
		
		// richtiger Osterhase --> von der Klasse
		System.out.println("down-casting");
		Osterhase neuerOsterhase = (Osterhase) oh2;
		neuerOsterhase.ostereier_verstecken();
		
		Weihnachtshase neuerWeihnachtshase = (Weihnachtshase) wh2;
		neuerWeihnachtshase.geschenke_verteilen();
	
		System.out.println();
		stall1.zeigeHasen();
	}

}
