package org.campus02.hase;

import java.util.ArrayList;

public class Hasenstall
{
	// das gleiche wie protected ArrayList<Hase> hasenstall = new ArrayList<>();
	ArrayList<Hase> hasenstall = new ArrayList<Hase>();
	
	public void addHase(Hase hase)
	{
		hasenstall.add(hase);
	}

	public void fressenHasen()
	{
		for (Hase hase : hasenstall)
		{
			hase.fressen();
		}
	}

	public void schlafenHasen()
	{
		for (Hase hase : hasenstall)
		{
			hase.schlafen();
		}
	}


	 
	public void zeigeHasen() // welche Hasen in meiner Liste sind Oster- oder Weihnachtseier?
	{
		for (Hase hase : hasenstall)
		{
			if(hase instanceof Osterhase)
			{
				((Osterhase) hase).ostereier_verstecken();
//				System.out.println("Diese Hasen verstecken Eier: " + hase.name);
			}
			if(hase instanceof Weihnachtshase)
			{
				((Weihnachtshase) hase).geschenke_verteilen();
//				System.out.println("Diese Hasen verteilen Geschenke: " + hase.name);
			}
			if(hase instanceof Osterhase)
			{
				Osterhase oh = (Osterhase) hase;
				oh.ostereier_verstecken();
			}
		}
	}
	
	
	public String toString()
	{
		String st = "";
		
		for (Hase hase : hasenstall)
		{
			st += "Name: " + hase.name + "\n";
		}
		return st;
	}



}
