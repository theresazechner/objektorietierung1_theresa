package org.campus02.hase;

public class Osterhase extends Hase
{

	public Osterhase(String name)
	{
		super(name);

	}

	public void ostereier_verstecken()
	{
		System.out.println(name + " versteckt Ostereier");
	}

	@Override
	public void schlafen()
	{
		System.out.println(name + " schl�ft von 16:00 bis 19:00 Uhr");
	}

	@Override
	public void fressen()
	{
		System.out.println(name + " frisst rote Ostereier");
	}

	
}
