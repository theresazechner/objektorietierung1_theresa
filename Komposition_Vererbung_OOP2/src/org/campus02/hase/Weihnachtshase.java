package org.campus02.hase;

public class Weihnachtshase extends Hase
{

	public Weihnachtshase(String name)
	{
		super(name);
		
	}

	public void geschenke_verteilen()
	{
		System.out.println(name + " verteilt Geschenke");
	}

	@Override
	public void fressen()
	{
		System.out.println(name + " frisst Weihnachtskugeln");
	}

	

}
