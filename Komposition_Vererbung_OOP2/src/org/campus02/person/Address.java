package org.campus02.person;

public class Address
{
	// wenn ich einen Konstruktor machen m�chte muss ich es von public auf private �ndern
	// Accessmodifiere
	public String strasse;
	public int plz;
	public String city;
	public int nr;
	

	
	public String getStrasse()
	{
		return strasse;
	}
	public void setStrasse(String strasse)
	{
		this.strasse = strasse;
	}
	public int getPlz()
	{
		return plz;
	}
	public void setPlz(int plz)
	{
		this.plz = plz;
	}
	public String getCity()
	{
		return city;
	}
	public void setCity(String city)
	{
		this.city = city;
	}
	public int getNr()
	{
		return nr;
	}
	public void setNr(int nr)
	{
		this.nr = nr;
	}
	
	
}
