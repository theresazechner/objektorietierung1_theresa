package org.campus02.person;

public class Customer extends Person
{
	public int id;
	private Address address;
	
	// �berladen von Konstruktor
	// man braucht default Konstruktor wenn man super weglassen m�chte
	public Customer(int id, Address address)
	{
		super("", "", "");
		this.id = id;
		this.address = address;
	}
	
	public Customer(int id, String age, Address address, String firstname, String lastname)
	{
		super(firstname, lastname, age); // Aufruf Konstruktor von SuperKlasse
		this.id = id;
		this.address = address;
	}

	public void changeAddress(Address address)
	{
		this.address = address;
	}
}
