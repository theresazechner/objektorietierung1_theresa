package org.campus02.person;

public class Person
{
	protected String firstname;
	private String lastname;
	private String age;
	
	public Person(String firstname, String lastname, String age)
	{
		this.age = age;
		this.firstname = firstname;
		this.lastname = lastname;
	}
	
	public String getFirstname()
	{
		return firstname;
	}

	public void setFirstname(String firstname)
	{
		this.firstname = firstname;
	}

	public String getLastname()
	{
		return lastname;
	}

	public void setLastname(String lastname)
	{
		this.lastname = lastname;
	}

	public String getAge()
	{
		return age;
	}

	public void setAge(String age)
	{
		this.age = age;
	}

	public void doSomething()
	{
		System.out.println(firstname + " " + lastname + " mache etwas!");
	}
}
