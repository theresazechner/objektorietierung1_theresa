package org.campus02.person;

public class PersonDemo
{

	public static void main(String[] args)
	{
		Address address = new Address();
		address.city = "Graz";
		Customer customer = new Customer(1, "35", address, "Mario", "M�ller"); 
		customer.doSomething();
		
		// geht so nicht!
//		a.strasse = "Hauptstrasse";
//		c.firstname = "Paul"; // Customer erbt von Klasse Person, daher kann ich auf firstname zugreifen
//		c.doSomething();
	}

}
