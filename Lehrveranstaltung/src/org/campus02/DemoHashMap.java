package org.campus02;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

public class DemoHashMap
{

	public static void main(String[] args)
	{
		// bei HashMap gibt es keine Schleifen
		HashMap<String, Integer> bezirkeMap = new HashMap<>();
		bezirkeMap.put("Graz", 273838);
		bezirkeMap.put("Wien", 500000);
		bezirkeMap.put("Deutschlandsberg", 60410);
		
		System.out.println("Key von HashMap");
		Set<String> keySet = bezirkeMap.keySet(); // keySet gibt den Key aus
		
		for (String bezirk : keySet)
		{
			System.out.println("Berzirk " + bezirk);
		}

		System.out.println();
		
		System.out.println("Value von HashMap");
		Collection<Integer> einwohnerZahl = bezirkeMap.values(); // value gibt collection zur�ck, value gibt nur den Wert aus
		
		for (Integer einwohner : einwohnerZahl)
		{
			System.out.println("Einwohnerzahl " + einwohner);
		}
	
		System.out.println();
		
		System.out.println("Key and Value von HashMap");
		Set<Entry<String, Integer>> keyValue = bezirkeMap.entrySet(); // entrySet gibt Key und Value aus , Set ist eine Collection
		
		//String keyValue = bezirkeMap.entrySet(); mit klick auf Fehlermeldung wird es automatisch in Set<Entry<String, Integer>> ...... umgewandelt!!!!
		
		for (Entry<String, Integer> entry : keyValue)
		{
			System.out.println("Bezirk " + entry.getKey() + " Einwohnerzahl:" + entry.getValue());
		}
		System.out.println();
		
		// Einwohnerzahl von Wien
		Integer w = bezirkeMap.get("Wien");
		System.out.println("Einwohnerzahl von Wien: " + w);
		
		System.out.println();
		
		System.out.println("Remove Key 'Wien' von HashMap");
		bezirkeMap.remove("Wien"); // l�scht den Bezirk Wien mit allen Daten --> Bezirk Wien ist weg
		for (Entry<String, Integer> entry : keyValue)
		{
			System.out.println("Bezirk " + entry.getKey() + " Einwohnerzahl:" + entry.getValue());
		}
	
		System.out.println();
		
		System.out.println("Clear die gesammte HashMap"); // die ganze HashMap wird gel�scht!!
		bezirkeMap.clear();
		for (Entry<String, Integer> entry : keyValue)
		{
			System.out.println("Ist die Map leer??");
		}
	}

}
