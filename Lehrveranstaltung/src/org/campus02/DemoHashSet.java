package org.campus02;
import java.util.HashSet;
import java.util.Iterator;

public class DemoHashSet
{
	public static void main(String[] args)
	{
		HashSet<String> hs = new HashSet<>();
		
		hs.add("string1");
		hs.add("string2");
		hs.add("string3");
		
		Iterator<String> iterator = hs.iterator();
		String newString = iterator.next();
		System.out.println("next() ohne Schleife " + newString);
		
		String newString2 = iterator.next();
		System.out.println("next() ohne Schleife " + newString2);
		
		System.out.println(" ---- Jetzt mit Schleife");
		
		iterator = hs.iterator();
		while(iterator.hasNext())
		{
			String str = iterator.next();
			System.out.println(str);
		}
		
		hs.remove("string1");
		
		System.out.println(" ---- Jetzt mit remove");
	
		iterator = hs.iterator();
		while(iterator.hasNext())
		{
			String str = iterator.next();
			System.out.println(str);
		}
		
		if(hs.contains("string3"))
		{
			hs.remove("string3");
		}
	
		System.out.println(" ---- Jetzt mit contains");
		iterator = hs.iterator();
		while(iterator.hasNext())
		{
			String str = iterator.next();
			System.out.println(str);
		}
	}
}
