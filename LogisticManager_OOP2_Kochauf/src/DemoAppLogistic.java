import org.campus02.logistic.Car;
import org.campus02.logistic.LogisticManager;
import org.campus02.logistic.Shirt;

public class DemoAppLogistic
{

	public static void main(String[] args)
	{
		Car car1 = new Car("VW", "Rot", 5000000);
		Car car2 = new Car("Audi", "Blau", 3500000);
		
		Shirt shirt1 = new Shirt("Burton", 38, "Blau");
		Shirt shirt2 = new Shirt("Michale Kors", 36, "Gelb");
		
		LogisticManager manager = new LogisticManager();
		
		car1.move("Graz");
		car2.move("Graz");
		
		shirt1.move("Graz");
		shirt2.move("Graz");

		manager.addObjekts(car1);
		manager.addObjekts(car2);
		manager.addObjekts(shirt1);
		manager.addObjekts(shirt2);
		
		System.out.println();
		
		manager.moveAll("Graz");
	}

}
