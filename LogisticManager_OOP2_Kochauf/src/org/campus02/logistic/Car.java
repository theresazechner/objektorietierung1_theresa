package org.campus02.logistic;

public class Car implements Moveable
{
	private String type;
	private String colour;
	private int weight;
	
	
	public Car(String type, String colour, int weight)
	{
		super();
		this.type = type;
		this.colour = colour;
		this.weight = weight;
	}


	public String getType()
	{
		return type;
	}


	public String getColour()
	{
		return colour;
	}


	public int getWeight()
	{
		return weight;
	}
	
	@Override
	public String toString()
	{
		return "Car [type=" + type + ", colour=" + colour + ", weight=" + weight + "]";
	}
	
	@Override
	public void move(String destination)
	{
		System.out.println(colour + " " +  type + " will be moved to " + destination);
	}
}
