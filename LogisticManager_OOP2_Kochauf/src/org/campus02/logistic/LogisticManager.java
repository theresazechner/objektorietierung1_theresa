package org.campus02.logistic;

import java.util.ArrayList;

public class LogisticManager
{
	private ArrayList<Moveable> objekteMoveable = new ArrayList<>();
	
	public void addObjekts(Moveable m)
	{
		objekteMoveable.add(m);
	}
	
	public void moveAll(String destination)
	{
		for (Moveable moveable : objekteMoveable)
		{
			moveable.move(destination);
		}
	}
}
