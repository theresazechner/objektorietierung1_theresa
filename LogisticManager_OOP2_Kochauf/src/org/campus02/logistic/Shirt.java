package org.campus02.logistic;

public class Shirt implements Moveable
{
	private String brand;
	private int size;
	private String colour;
	
	public Shirt(String brand, int size, String colour)
	{
		super();
		this.brand = brand;
		this.size = size;
		this.colour = colour;
	}

	public String getBrand()
	{
		return brand;
	}

	public int getSize()
	{
		return size;
	}

	public String getColour()
	{
		return colour;
	}

	@Override
	public String toString()
	{
		return "Shirt [brand=" + brand + ", size=" + size + ", colour=" + colour + "]";
	}
	
	@Override
	public void move(String destination)
	{
		System.out.println(colour + " " + " " + brand + " Shirt will be moved to " + destination);
	}
}
