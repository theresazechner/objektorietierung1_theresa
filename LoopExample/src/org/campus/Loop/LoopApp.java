package org.campus.Loop;

public class LoopApp 
{

	public static void main(String[] args) 
	{
		double result_ue1_2 = sumBruch(100);
		System.out.println(result_ue1_2);
		
		int result_ue2_3_a = sum(6);
		System.out.println(result_ue2_3_a);
		
		int result_ue2_3_b = sum2(9000);
		System.out.println(result_ue2_3_b);
		
	}	
		
	public static double sumBruch(int counter)
	{
	
		double result = 0;
		
		for (double index = 2; index <= counter; index++)
		{
			result = result + 1 / index;
		}
		return result;
	}

	public static int sum(int counter)
	{
		int result = 0;
		for(int index = 0; index<= counter; index = index + 3)
		{
			result = result + index;
		}
		return result;
	}

	public static int sum2(int counter)
	{
		int result = 0;
		for(int index = 3; index<= counter; index = index + 3)
		{
			result = result + index;
		}
		return result;
	}
}





/*1. Erstellen Sie in einem neuen Projekt (LoopExample) eine Klasse LoopApp mit
     einem main Methode.
2. Schreiben Sie ein Programm mit dem die Reihe � + 1/3 + � .... 1/100 summiert
   wird und geben Sie das Ergebnis in der Console aus.
3. Schreiben Sie in der selben Klasse eine Programm in dem die Reihe
   3+6+9+12+15 ... 9000 summiert wird und geben Sie das Ergebnis auf der Konsole
   aus.
*/