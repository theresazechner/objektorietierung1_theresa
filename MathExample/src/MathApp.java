
public class MathApp
{

	public static void main(String[] args)
	{
		double [] zahl = new double [] {1, 2, 3, 4, 5, 6, 7, 8, 156};
		
		int [] findeZahlen = new int [] {1, 2, 5, 10, 5, 6, 7, 8, 1};
		
		int sucheZahl = 5;
		
		ziffernsumme(623);
		mittelwert(zahl);
		findeZahl(findeZahlen);
		kleinsteZahl(findeZahlen);
		int result = wieOftZahl(findeZahlen, sucheZahl); // die sachen in der klammer werden an die methode �berliefert/ �bergeben 
														 // int result = ... �bernimmt den R�ckgabewert der Methode
		
		System.out.println("Die Zahl " + sucheZahl + " kommt " + result + " mal vor!");
		
	}

	
	public static void ziffernsumme(int zahl)
	{ 
		int summe = 0;
		int ziffer;
		
		
		while (zahl > 0)
		{
			ziffer = zahl % 10; // modulo 3 wird auf der Ziffer gespeichert
			
			summe = summe + ziffer; // im ersten schritt ist summe 0 und 0+ ziffer 3 = 3 
			
			zahl = zahl / 10; // 623 7 10 ist 62 --> wird wieder oben hingegebn 
							  // im n�chsten schritt ist dann modulo von 62 --> 2
							  // wird wieder gespeichert und 3 + 2 = 5
			
		}
		
		System.out.println(summe);
	}

	public static double mittelwert(double zahlen[])
	{
		double result = 0;
	
		for( int index = 0; index < zahlen.length; index++) // erh�he index um 1, bis alle zahlen im Array "besucht" werden
		{
			result = result + zahlen[index]; // damit er wei� was an der stelle steht --> zahlen[index] = welche zahl steht auf der position im Array?
		}
	
		result = result / zahlen.length; // dividiere die Summe aller Zahlen im Array durch die Anzahl der Positionen  im Array
		
		System.out.printf("%.2f" , result);
		
		return result;
	}

	public static int findeZahl(int max[]) // int weil ganzzahl
	{
		
		int result = max[0]; // nimmt die Zahl an der Stelle 0 im Array
		
		for(int index = 0; index < max.length; index++) // Vorsicht bei for Schleife --> wo ich anfang zu z�hlen und wie weit es geht
		{
								
			if(result < max[index])
			{
				result = max[index];
			}		
	
	   }
			
	    System.out.println(result);
	    System.out.println(result);
		return result;
	
    }
	
	public static int kleinsteZahl(int min[]) // Array [] nicht vergesen!!!!!
	{
		int result = min[0];
		
		for(int index = 0; index < min.length; index ++)
		{
			if(result > min[index]) // minimum von meienm index
			{
				result = min[index];
			}
		}
		
		System.out.println(result);
		return result;
	}

	public static int wieOftZahl(int zahlen[], int sucheZahl)
	{
		int result = 0;
		
		for(int index = 0; index < zahlen.length; index++)
		{
			if(zahlen[index] == sucheZahl)
			{
				result = result + 1;
			}
		}
		
		
		return result;
	}



}	


/*1. Erstellen Sie in einem neuen Projekt (MathExample) eine Klasse MathApp mit
     einem main Methode.
2. Schreiben Sie eine Methode, welche die Ziffernsumme einer Zahl berechnet und
   geben sie das Ergebnis auf der Konsole aus. 623 -> 6+2+3 = 11
3. Schreiben Sie eine Methode die den arithmetischen Mittelwert der Zahlen in
   einem Feld berechnet und zur�ck gibt. Geben Sie das Ergebnis auf der Konsole
   aus.
4. Schreiben Sie Methoden um die kleineste und gr��te Zahl in einem Feld zu
   finden.
5. Schreiben Sie eine Methode die zur�ck gibt, wie oft eine gewisse Zahl ein einem
   Feld vorkommt und geben Sie das Ergebnis in der main Methode auf der Console
   aus.
*/