
public class DemoApp
{

	public static void main(String[] args)
	{
		MayBeInt beInt1 = new MayBeInt(10, 0);
		System.out.println(beInt1.print());
		
		MayBeInt beInt2 = new MayBeInt(100, 1);
		System.out.println(beInt2.print());
		
		MayBeInt beInt3 = new MayBeInt(50, 2);
		System.out.println(beInt3.print());

	}

}
