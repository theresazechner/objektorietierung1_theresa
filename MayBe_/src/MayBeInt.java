
public class MayBeInt
{
	private int data;
	private int status;
	
	public MayBeInt(int data, int status)
	{
		this.data = data;
		this.status = status;
	}

	public int getData()
	{
		return data;
	}

	public void setData(int data)
	{
		this.data = data;
	}

	public int getStatus()
	{
		return status;
	}

	public void setStatus(int status)
	{
		this.status = status;
	}

	public String print()
	{
		switch (status)
		{
		case 1:
			return "Zugriff gestattet: " + data; // ich brauch kein break da ich ein return verwende --> zeitsparender so! bei break m�sste ich wieder eine Variable definieren
		
		case 2:	
			return "Zugriff nicht gestattet!";
		
		case 3:	
			return "Nicht erfasst";	

		default: 
			return "Ung�ltiger Status!";
		}
	}
}
