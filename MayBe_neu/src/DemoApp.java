import java.util.GregorianCalendar;

public class DemoApp
{

	public static void main(String[] args)
	{
		Profil pSusi = new Profil("Susi", 
				"Graz", 
				27, 
				new MayBe<String>("susi@gmail.com"),
				'F', 
				new MayBe<GregorianCalendar>(new GregorianCalendar(1990, 01, 15)),
				new  MayBe<Double>(2000.00));

		// so mach ich das wenn ich im Konstruktor nicht so wie ich definiert hab --> andere Version(nicht meine) ist viel einfacher!!
//		Profil pSusi = new Profil("Susi", "Graz", 27, "susi@gmailcom", "F", new GregorianCalendar(1990, 01, 15), 2000.00)
		
		pSusi.setSalaryStatus(0);
		
		pSusi.print();
	}

}
