import java.util.GregorianCalendar;

public class Profil
{

	private String name; 
	private String address;
	private int age;
	private MayBe<String> email;
	private char gender; 
	private MayBe<GregorianCalendar> dateOfBirth;
	private MayBe<Double> salary;
	
	public Profil(String name, String address, int age, MayBe<String> email, char gender,
			MayBe<GregorianCalendar> dateOfBirth, MayBe<Double> salary)
	{
//		this.email = new MayBe<String>(email); das schreibe ich so wenn ich in meinem Konstruktor statt MayBe<String> email
//		nur String email schreib --> meine Variante ist nicht falsch in meinem Fall wei� die DemoApp von meinem MayBe
//		geht eine Klasse weiter --> onhne MayBe ist es abgekapselt und meine DemoApp wei� nichts von dem!!
		
	
		this.name = name;
		this.address = address;
		this.age = age;
		this.email = email;
		this.gender = gender;
		this.dateOfBirth = dateOfBirth;
		this.salary = salary;
	}

	public void setSalaryStatus(int status)
	{
		this.salary.setStatus(status); // ich ruf die setStatus Methode von MayBe auf , wir setzen den Status zu salery hin
	}

	public void print()
	{
		String text = "Profil name: " + name + " address: " + address + " age: " + age + " gender: " + gender;
		System.out.println(text);
		this.email.print();
		this.dateOfBirth.print();
		this.salary.print();
	}
}
