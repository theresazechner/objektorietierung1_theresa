package org.campus02.mitarbeiter;

public class DemoAppEmployee
{

	public static void main(String[] args)
	{
		FixCommisionEmployee emp1= new FixCommisionEmployee("Max", "Mustermann", "Marketing", 3000, 350);
		PercentCommisionEmployee emp2 = new PercentCommisionEmployee("Susi", "Sorglos", "Sales", 3500, 5);		
		FixCommisionEmployee emp3 = new FixCommisionEmployee("Jonny", "Manager", "Controlling", 4000, 1000);		
	
	
		EmployeeManager empManger = new EmployeeManager();
		empManger.addEmployee(emp1);
		empManger.addEmployee(emp2);
		empManger.addEmployee(emp3);
		
		System.out.println("TotalSalary: "+ empManger.calcTotalSalary());
		
		System.out.println("Salary by Department: " + empManger.getSalaryByDepartment());
	
	}

}
