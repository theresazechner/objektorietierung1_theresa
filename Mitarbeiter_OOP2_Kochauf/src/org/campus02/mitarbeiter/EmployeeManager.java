package org.campus02.mitarbeiter;

import java.util.ArrayList;
import java.util.HashMap;

public class EmployeeManager
{
	private ArrayList<Employee> listMitarbeiter = new ArrayList<>();
	
	public void addEmployee(Employee e)
	{
		listMitarbeiter.add(e);
	}
	
	public double calcTotalSalary()
	{
		double result = 0;
		for (Employee employee : listMitarbeiter)
		{
			result += employee.getFullSalary();
		}
		return result;
	}

	public HashMap<String, Double> getSalaryByDepartment()
	{
		// Double und String sind Referenzdatentypen
		HashMap<String, Double> abteilungsListe = new HashMap<>();
		// mein Code
		for (Employee employee : listMitarbeiter)
		{
			// Überprüfen
			if(abteilungsListe.containsKey(employee.getDepartement()))
			{
				// auslesen, bearbeiten, ablegen
				double value = abteilungsListe.get(employee.getDepartement()); // auslesen
				value = value + employee.getFullSalary(); // bearbeiten
				abteilungsListe.put(employee.getDepartement(), value); // ablegen
			}
			else
			{
				// Department noch nicht in HashMap
				abteilungsListe.put(employee.getDepartement(), employee.getFullSalary());
				// wenn Dep noch nicht in HashMap --> lege in Key und Value ab
			}
		}
		return abteilungsListe;
	}
}
