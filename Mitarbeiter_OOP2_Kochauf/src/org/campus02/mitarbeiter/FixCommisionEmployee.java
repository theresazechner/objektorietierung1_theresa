package org.campus02.mitarbeiter;

public class FixCommisionEmployee extends Employee
{
	private double additionalCommision;

	public FixCommisionEmployee(String lastname, String firstname, String departement, double baseSalary,
			double additionalCommision)
	{
		super(lastname, firstname,departement, baseSalary);
		this.additionalCommision = additionalCommision;
	}
	
	@Override
	public double getFullSalary()
	{
		return baseSalary + additionalCommision;
	}
	
	public double getAdditionalCommision()
	{
		return additionalCommision;
	}

	@Override
	public String toString()
	{
		return "FixCommisionEmployee [additionalCommision=" + additionalCommision + "]";
	}

	
}
