package org.campus02.mitarbeiter;

public class PercentCommisionEmployee extends Employee
{
	private double percentCommision;

	public PercentCommisionEmployee(String lastname, String firstname, String departement, double baseSalary,
			double percentCommision)
	{
		super(lastname, firstname, departement, baseSalary);
		this.percentCommision = percentCommision;
	}
	
	@Override
	public double getFullSalary() // ‹berschreiben einer Methode
	{
		return baseSalary + baseSalary / 100.0 * percentCommision;
	}

}
