import org.campus.personen.Person;

public class PersonenApp
{

	public static void main(String[] args) //args = Name vom Parameter; main = �ffentlich und statisch
	{
		Person p1 = new Person("Luke", "Skywalker");
		System.out.println(p1);
		
		Person p2 = new Person ("Anakin","Skywalker");
		System.out.println(p2);
		
		Person p3 = new Person ("Padme","Amidala");
		Person p4 = new Person("Obiwan", "Kenobi");
		
		p1.setVater(p2); // "Luke ich bin dein Vater *chh chhrh chh*"
		p1.setMutter(p3);;
		
		System.out.println(p1); // keine R�ckgabewert schreibt nur auf Console, out = Membervariable ist kursiv weil es auf static ist
		System.out.println(p2);
		System.out.println(p3);
		System.out.println(p4);
		
		Math.random(); // statische Methode der Kalsse Math. // Hilfsmethode --> hat keinen inneren Wert
		// static bezieht sich auf kein Objekt --> Art Hilfsmethode
		// gibt nur einmal pro Klasse kann ich auch benutzen ohne das ich Objekt gemacht hab (new)
		// static sind immer kursiv
		// blau = Membervariable
		// System.out // statische �ffentliche Membervariable von System
		
		System.out.println(p1.getVater()); // Getter von Person.java --> das ist die Ausgabe

	}

}
