package org.campus.personen;

public class Person
{
	private String vorname;
	private String nachname;
	private Person mutter; 
	private Person vater;
	
	private static int anzahlPersonen; // alleObjekte teilen sich die gleiche Variable --> gibt es nur einmal pro Klasse
										// andere Variablen gibt es f�r jedes Objekt static gibt es nur einmal
	
	public Person(String vorname, String nachname)
	{
		this.vorname = vorname;// this ist ein verweis auf mich selbst --> so kann ich den Datentyp nicht vertauschen 
		this.nachname = nachname;//und ich kann f�r den Parameter den gleciehn Namen benutzen wie in der Class!
		anzahlPersonen++; // nur einen Variable 
	}

	public void setVater(Person vater)
	{
		this.vater = vater;
	}
	
	public void setMutter(Person mutter) // set = einlesen --> Setter art Methode --> Wert der von au�en kommt im Objekt zu merken
										// wenn ich eine Person mitgeben will muss im Parameter Person stehen
										// wenn ich will das sie ver�ndert werden k�nnen nimmmt man einen Setter
										// welche informationen will ich preis geben mit Setter und Getter
	{
		this.mutter = mutter;
	}
	
	public Person getVater() // get = ausgeben --> Getter --> anfragen 
							// ich muss werder Setter noch Getter nehmen
							// get wenn ich fargen m�chte  Luke wer ist dein Vater
							// get ist zum auslesen 
	{	
	
		return vater;
	}

	public String toString()
	{
		//return vorname + "" + nachname; // den Namen von der Klasse nehmen --> beides beteuted das gleiche
										// oder return String.format("%s %s", Vorname , Nachname); --> %s bedeutet String
		return String.format("(%s %s(%s %s)) %d",vorname, nachname, mutter, vater, anzahlPersonen); // %d steht f�r Ganzzahl
										// %s sind Platzhalter --> da kommen sonst mutter und vater rein da ich die aber nicht kenne kommt null rein
	}

}
