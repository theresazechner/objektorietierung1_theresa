package org.campus02.asciiart;

import java.util.Scanner;

public class DemoAppShape
{
	public static void main(String[] args)
	{
		System.out.print("Welches Array m�chten Sie ausgeben? Eingabe m�glich von a - f: ");
		
		Scanner eingabe = new Scanner(System.in);

		String vonConsole = eingabe.nextLine();
		
		System.out.print("W�hlen Sie Ihrer Skalierung - sie muss ein vielfaches von 3 sein:");
		
		Scanner eingabeSkal = new Scanner(System.in);
		String SkalvonScanner = eingabeSkal.nextLine();
		int size = Integer.parseInt(SkalvonScanner);
	
		eingabe.close();
		eingabeSkal.close();
		
		if(size %3 == 0)
		{	
		switch (vonConsole)
		{
		case "a": ShapeA a = new ShapeA(size);
		System.out.println(a);
			break;
			
		case "b": ShapeB b = new ShapeB(size);
		System.out.println(b);
			break;
			
		case "c": ShapeC c = new ShapeC(size);
		System.out.println(c);
			break;
			
		case "d": ShapeD d = new ShapeD(size);
		System.out.println(d);
			break;
			
		case "e": ShapeE e = new ShapeE(size);
		System.out.println(e);
			break;
			
		case "f": ShapeF f = new ShapeF(size);
		System.out.println(f);
			break;
			
		default: System.out.println("Eingabe ist falsch!");	
			break;
		}
	  }
		else
		{
			System.out.println("Falsche Eingabe Skalierung!");
		}
	}
	
}
