package org.campus02.asciiart;

public abstract class Shape
{
	protected int size;
	
	protected char[][] shape; // char dewegen weil ich einzelne Buchstaben reingebe!!

	
	public Shape(int size)
	{
		this.size = size;
		this.shape = new char[size][size];
		
	}
	
	
	@Override
	public String toString()
	{
		String result = "";
		for(int i = 0; i < shape.length; i++)
		{
			for(int a = 0; a < shape.length; a++ )
			{
				result += shape[i][a] + "\t"; 
			}
			result += "\n";
		}
		return result;
	}
	
	
}
