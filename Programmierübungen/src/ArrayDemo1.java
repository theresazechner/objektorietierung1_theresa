
public class ArrayDemo1
{

	public static void main(String[] args)
	{
		int [] array1 = new int [] {3,4,5};
		int [] array2 = new int [] {7,1,3};
		
		System.out.println("Summe und Produkt: ");
		sum(array1, array2); // nur wenn die Methode static ist kann ich auf sie in der main Methode zugreifen!!!
		
//		System.out.println("Summe: ");
//		summe(array1, array2);
//		System.out.println();
//		System.out.println("Produkt: ");
	}

	// Variante 1
//	public static void summe(int array1[], int array2[])
//	{
//		int result = 0;
//		for (int zahl = 0; zahl < array1.length; zahl++ )
//		{
			
//			result = array1[zahl] + array2[zahl];
		
//			System.out.println(result + " ");
//		}
	
//		System.ou.println();
//		for (int zahl = 0; zahl < array1.length; zahl++ )
//		{
			
//			System.out.println(array1[zahl] * array2[zahl] + " ");
//		}
//	}
	
	// Variante 2
	public static void sum(int array1[], int array2[]) // static darum kann ich oben zugreifen!!
	{
		for (int zahl = 0; zahl < array1.length; zahl++ )
		{
//			int summe  = array1[zahl] + array2[zahl]; // --> ist das gleiche wie unten
//			System.out.println(summe);
		
			System.out.print(array1[zahl] + array2[zahl] + " "); // --> gleiche wie oben nur verk�rzte Schreibweise
		}
			System.out.println(); // wenn ich ein leeres System.out.println(); mache dann macht er mir eine Leerzeile
	
		for (int zahl = 0; zahl < array1.length; zahl++ )
		{
			
			System.out.print(array1[zahl] * array2[zahl] + " ");
		}
	}
}
