
public class Matrix
{

	public static void main(String[] args)
	{
		// so h�tte ich es gemacht
//		generateIdentityMatrix(5);
		
		double[][] matrix = generateIdentityMatrix(5);
		
		 printMatrix(matrix);
		
	}

	public static double[] [] generateIdentityMatrix(int size) // ich gebe kein Array von au�en mit sonder sag in der Methode das ich ein Array haben m�chte --> darum erstelle ich es in der Methode und nicht in der mainMethode
	{
//		double[][] matrix = new double [] [] {{1.0},{0.0}}; --> so h�tte ich es geschrieben
		double[][] matrix = new double [size] [size]; // ich hab 5 Arrays und es ist 2 dimensional
		
		//mein Versuch
//		for(double index = 0; index < size; index++)
//		{
//			System.out.println(matrix);		
//		}
		
		for(int index = 0; index < matrix.length; index++)
		{
			matrix[index][index] = 1.0;
		}
		return matrix;
	}
	
	public static void printMatrix(double[][] matrix)
	{
		for(int i = 0; i < matrix.length; i++)
		{
			for(int a = 0; a < matrix.length; a++ )
			{
				System.out.print(matrix[i][a] + "\t"); // --> "\t" hei�t das ein abstand zwischen den einzelnen Zahlen ist
			}
			System.out.println();
		}
	}
}
