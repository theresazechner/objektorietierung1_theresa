
public class Metropolis
{

	public static void main(String[] args)
	{
//		boolean hauptstadt = true;
//		int einwohner = 150_000;
//		double taxPerPersonAndMonth = 430.0; --> das brauch ich nicht doppelt angeben weil ich es eh bei boolean result = isMetropolis (...)
		
		boolean result = isMetropolis(true, 150000, 430.0); // ich gebe hier gleich meine werte mit
		System.out.println("Metropolis? " + result);

	}

	public static boolean isMetropolis(boolean isCapital, int inhabitants, double taxPerPersonAndMonth) // variablen in der main Methode haben keine Beziehung zu den Variablen in der isMetropolis Methode
	{
		// das ist meine Variante
//		boolean result = true; 
		
//		if(inhabitants > 100000 == isCapital) 
//		{
//			return result;
//		}else if(inhabitants > 200000 && taxPerPersonAndMonth *12 * inhabitants >= 720000000)
//		{
//			return result;
//		}else
//		{
//			return false;
//		}
		
		return((isCapital && inhabitants > 100_00) // Variante von Frau Mandl --> ist nicht so verschachtelt weil ich kein else if und noch ein else brauch!
				|| (inhabitants > 200_000
						&& taxPerPersonAndMonth * 12 * inhabitants >= 720_000_000));
		
	}
	
	
}
