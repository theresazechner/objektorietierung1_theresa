import org.campus.uebungen.Person;

public class PersonenApp
{
	public static void main(String[] args)
	{
	
	Person person1 = new Person ("Max" , "Mustermann" , 39);
	Person person2 = new Person ("Susi" , "Musterfrau", 35);
	Person person3 = new Person ("Lili" , "Musterfrau", 5);
	
//	Person person4 = new Person(); --> weil ich leeren Knstruktor angelegt hab kann ich auch die person ohne Parameter anlegen!
	
	System.out.println(person1);
	System.out.println(person2);
	System.out.println(person3);
//	System.out.println(person4);
	}
}
