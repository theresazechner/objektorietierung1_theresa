
public class TeilerFinden
{

	public static void main(String[] args)
	{
		int zahl = 128; // von Frau Mandl
		findeTeiler(zahl); // von Frau Mandl
		
		teiler(2, 128);
		teiler1(2, 128);
	}

	// von Frau Mandl
	public static void findeTeiler(int zahl)
	{
		int teilerKanditat = 2;
		
		String ausgabeAlleTeiler = "teiler von" + zahl + ":";
		
		while(teilerKanditat <= zahl / 2) { // markieren strg + shift + I dann sieht man ob die Bedingung true ist oder nicht -- > geht nur im depuger
			if(zahl % teilerKanditat == 0) {
				ausgabeAlleTeiler += teilerKanditat + ","; // verk�rzte Schreibweise --> ausgabeAlleTeiler = ausgabeAlleTeiler + teilerKandidat + ",";
			}
			
			teilerKanditat++; // teilerKanditat brauch sonst gibt es endlosschleife
//			teilerKanditat = +2; --> so geht er weniger oft durch weil er dann immer um 2 erh�ht und nicht nur um 1!
		}
	
		System.out.println(ausgabeAlleTeiler);
	
	}
	
	
	
	// mein Versuch --> zu Hause durchdenken
	public static int teiler(int teiler, int ganzzahl)
	{
		int result = 0;
		while(ganzzahl % 2 == 0)
		{
			teiler = ganzzahl / 2;
			System.out.println(teiler);
		}
		
		return result;
	}
	
	// andere L�sung
	public static void teiler1(int teilen, int ganzzahl)
	{
		while(teilen < ganzzahl )
		{
			if(ganzzahl % teilen == 0)
			{
				System.out.println("Teiler von " + ganzzahl + ": " + teilen);
			}
			
			teilen ++;
		}
		
	}
}
