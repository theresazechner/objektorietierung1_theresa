package org.campus.uebungen;

public class Person
{
	// private deswegen damit keiner von au�en auf meine Attribute zugreifen kann!
	// sie sollen nicht ver�ndert werden darum private
	// Membervariablen
	private String vorname; 
	private String nachname;
	private int alter;
	
//	public Person() // Ich mach leeren Konstruktor damit ich eine person anlegen kann ohne Parameter
//	{
		
//	}
	
	public Person(String vorname, String nachname, int alter) // Konstruktor
	{

		this.vorname = vorname;
		this.nachname = nachname;
		this.alter = alter;

	}
	
	public String toString() // Source --> Generate toString...
	{
//		return String.format("%s %s, %d Jahre alt", vorname, nachname, alter); --> wenn ich es so schreibe erspare ich mir das mit dem  + und " "
		return vorname + " " + nachname + " , " + alter + " Jahre alt";
	}

}
