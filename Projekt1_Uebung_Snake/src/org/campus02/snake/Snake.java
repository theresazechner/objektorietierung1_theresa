package org.campus02.snake;

public class Snake extends Spielfeld
{
	private char s;
	private int x;
	private int y;
	

	public Snake(int x, int y)
	{
		super();
		this.s = 'S';
		this.x = x;
		this.y = y;
		field[x][y] = s;
	}

	public void moveLeft()
	{
		if(field[x][y - 1] == 'X') // X --> Rand des Spielfeldes --> er soll gehen solange kein X sprich das Spielfeldende ist
		{
			field[x][y] = s;
		}
		else
		{
		field[x][y] = ' ';
		y = y -1;
		field[x][y] = s;
		}
		//mein Versuch
//		char down = field[5][5];
//		for(int i = 5; i < field.length; i++)
//		{
//			down += field[5][i];
//		}
		
	}

	public void moveAbove()
	{
		if(field[x -1][y] == 'X')
		{
			field[x][y] = s;
		}
		else
		{
		field[x][y] = ' ';
		x = x -1;
		field[x][y] = s;
		}
	}

	public void moveRight()
	{
		if(field[x][y + 1] == 'X')
		{
			field[x][y] = s;
		}
		else
		{
		field[x][y] = ' ';
		y = y + 1;
		field[x][y] = s;
		}
	}

	public void moveDown()
	{
		if(field[x + 1][y] == 'X')
		{
			field[x][y] = s;
		}
		else
		{
		field[x][y] = ' ';
		x = x + 1;
		field[x][y] = s;
		}
	}

}
