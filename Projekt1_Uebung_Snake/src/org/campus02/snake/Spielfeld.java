package org.campus02.snake;

public class Spielfeld
{

	protected char[][] field = new char[10][10];

	public Spielfeld()
	{
		for(int i = 0; i < field.length; i++)
		{
			field[0][i] = 'X';
			field[i][0] = 'X';
			field[i][9] = 'X';
			field[9][i] = 'X';
			
		}
	
	}
	@Override
	public String toString()
	{
		String result = "";
		for(int i = 0; i < field.length; i++)
		{
			for(int a = 0; a < field.length; a++ )
			{
				result += field[i][a]; 
			}
			result += "\n";
		}
		return result;
	}
	
	
}
