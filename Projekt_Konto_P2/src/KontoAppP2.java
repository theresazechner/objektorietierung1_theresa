import org.campus.konto.Konto;

public class KontoAppP2
{

	public static void main(String[] args)
	{
		Konto konto1 = new Konto();
		
		konto1.setInhaber("Susi");
		
		konto1.aufbuchen(100.0);
		
		konto1.abbuchen(11.50);
		
		System.out.println("Der Kontostand vom Konto1 betr�gt: " + konto1.getKontostand() + " Euro");
		
		// so hab ich es gemacht
		/*kontoinhaber1.aufbuchen(2000, 0);
		System.out.println(kontoinhaber1);
		
		kontoinhaber1.abbuchen(2000, 1000);
		System.out.println(kontoinhaber1);
		*/
	}

}
