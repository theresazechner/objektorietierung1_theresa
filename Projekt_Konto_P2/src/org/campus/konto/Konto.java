package org.campus.konto;

public class Konto
{
	private String kontoinhaber;
	private double kontostand;
	
	// (ich brauch keine Konstruktor)
//	public Konto(String kontoinhaber, int kontostand)
//	{
//		this.kontoinhaber = kontoinhaber;
//		this.kontostand = kontostand;
	
//	}

//	public static void setInhaber(String kontoinhaber, int kontostand)
//	{
//		this.kontoinhaber = kontoinhaber;
//		kontostand = 0;
//	}

	public void setInhaber(String kontoinhaber)
	{
		this.kontoinhaber = kontoinhaber;
		this.kontostand = 0;
	}
	
	public void aufbuchen(double wert)
	{
		this.kontostand +=wert;
	}
	
	public void abbuchen(double wert)
	{
		if(this.kontostand - wert > 0)
		{
			this.kontostand -=wert;
		}else
		{
			System.out.println("Nicht genug Geld am Konto!");
		}
	}
	
	public double getKontostand()
	{
		return this.kontostand;
	}
	
	// mein Versuch --> mit oder ohne return Wert das ist egal --> geschmackssache!
/*	public static double aufbuchen(int geldbetrag,int kontostand)
	{ 
		double result = 0;

		
		for(double index = 0; index < geldbetrag; index++)
		{
			result = kontostand + geldbetrag;
		}
	
		return result;	
	}

	public static double abbuchen(int kontostand, int geldbetrag)
	{
		double result = 0;
		
		for(double index = 0; index < geldbetrag; index++)
		{
			result = kontostand - geldbetrag;
		}
		
		return result;
		
		toString Methode noch hinzufügen!!
	*/

}
