
public class Zeichenketten 
{
	public static void main(String[] args)
	{
		String letter = "Saettigkeitsgefuehl*17*"; // Zielergebnis f�r A1 = 21
		
		String letter1 = "Hansi5_";
		
		String letter2 = "Zebra88.";
		
//		System.out.println(buchstaben.charAt(0));
	
		konvertieren(letter);
		System.out.println();
		konvertieren(letter1);
		System.out.println();
		konvertieren(letter2);
	}
	
	public static void konvertieren(String buchstaben)
	{
		for(int index= 0; index < buchstaben.length(); index++)
		{
			//System.out.println(buchstaben.charAt(index));
			char einzelbuchstabe = buchstaben.charAt(index); // ich hab dem einzelbuchstaben den buchstaben.chartAt(index) zugewiesen damit die schreibweise k�rzer wird
			char zahl = buchstaben.charAt(index);
//			char zeichen = buchstaben.charAt(index); --> brauch ich nicht
//			zeichen = '_';
			
			//ich arbeite mit if und else if weil ich ja sage wenn (if) es das ist sonst (else if) ist es etwas anderes --> so h�ngt alles zusammen --> nur mit if fragt er alles doppelt und dreifach ab
			if(einzelbuchstabe == 'A' || einzelbuchstabe == 'a' || einzelbuchstabe == 'B' || 
			   einzelbuchstabe == 'b' || einzelbuchstabe == 'C' || einzelbuchstabe == 'c') 
			{
				System.out.print("2");	
			}
		
			else if(einzelbuchstabe == 'D' || einzelbuchstabe == 'd' || einzelbuchstabe == 'E' || 
					einzelbuchstabe == 'e' || einzelbuchstabe == 'F' || einzelbuchstabe == 'f') 
			{
				System.out.print("3");
			}	

			else if(einzelbuchstabe == 'G' || einzelbuchstabe == 'g' || einzelbuchstabe == 'H' || 
					einzelbuchstabe == 'h' || einzelbuchstabe == 'I' || einzelbuchstabe == 'i') 
			{
				System.out.print("4");
			}	
		
			else if(einzelbuchstabe == 'J' || einzelbuchstabe == 'j' || einzelbuchstabe == 'K' || 
					einzelbuchstabe == 'k' || einzelbuchstabe == 'L' || einzelbuchstabe == 'l') 
			{
				System.out.print("5");
			}	
		
			else if(einzelbuchstabe == 'M' || einzelbuchstabe == 'm' || einzelbuchstabe == 'N' || 
					einzelbuchstabe == 'n' || einzelbuchstabe == 'O' || einzelbuchstabe == 'o') 
			{
				System.out.print("6");
			}	
		
			else if(einzelbuchstabe == 'P' || einzelbuchstabe == 'p' || einzelbuchstabe == 'Q' || 
					einzelbuchstabe == 'q' || einzelbuchstabe == 'R' || einzelbuchstabe == 'r' || 
					einzelbuchstabe == 'S' || einzelbuchstabe == 's') 
			{
				System.out.print("7");
			}	
			
			else if(einzelbuchstabe == 'T' || einzelbuchstabe == 't' || einzelbuchstabe == 'U' || 
					einzelbuchstabe == 'u' || einzelbuchstabe == 'V' || einzelbuchstabe == 'v') 
			{
				System.out.print("8");
			}	
			
			else if(einzelbuchstabe == 'W' || einzelbuchstabe == 'w' || einzelbuchstabe == 'X' || 
					einzelbuchstabe == 'x' || einzelbuchstabe == 'Y' || einzelbuchstabe == 'y' || 
					einzelbuchstabe == 'Z' || einzelbuchstabe == 'z') 
			{
				System.out.print("9");
			}	
		
			else if( zahl =='0' || zahl == '1' || zahl =='2' || zahl == '3' || zahl == '4' || 
					 zahl == '5' || zahl == '6' || zahl == '7' || zahl == '8' || zahl == '9')
			{
				System.out.print(zahl);
			}
			
			// erspare ich mir indem ich alles mit else if und am schluss else mache
//			if(zeichen == '.' || zeichen == ',' || zeichen == ';' || zeichen == ':' || zeichen == '_' || zeichen == '-' || zeichen == '#' || zeichen == '+' 
//					|| zeichen == '*' || zeichen == '~' || zeichen == '�' || zeichen == '`' || zeichen == '?' || zeichen == '=' || zeichen == '}' || zeichen == '{' 
//					|| zeichen == ']' || zeichen == '[' || zeichen == ')' || zeichen == '(' || zeichen == '/' || zeichen == '&' || zeichen == '%' || zeichen == '$' 
//					|| zeichen == '�' || zeichen == '!' || zeichen == '�' || zeichen == '^' || zeichen == '<' || zeichen == '>' || zeichen == '|' || zeichen == '@' || zeichen == '�')
//			{
//				System.out.println('_');
//			}
			else
			{
				System.out.print('_');
			}
//			else
//			{
//				System.out.print(zeichen);
//			}
		}
	}
}



/*Schreiben Sie ein Programm1 mit dem eine Zeichenkette in eine andere Zeichenkette
konvertiert wird. Die neue Zeichenkette soll die Zahlen beinhalten, die man auf einem
Mobiltelefon dr�cken muss, wenn man einen Text eingibt. Ber�cksichtigen Zahlen und
Gro�buchstaben (keine Sonderzeichen wie �). Alle anderen Zeichen sollen als
Unterstrich '_' zur�ck gegeben werden (z.B: �Hansi75� w�rde zu 4267475). Zeigen
Sie das anhand von 3 verschiedenen Zeichenketten.2
*/