package at.campus02.nowa.swe;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner scanner = null;
		Game game = new Game(10);
		System.out.println("Steuern Sie die Schlange mit 4,8,6,2 auf dem Nummernpad");

		try {
			scanner = new Scanner(System.in);
			int direction = 0;

			System.out.println(game);
			while (true) {
				
				do {
					direction = scanner.nextInt();
				}
				while (direction != 4 && direction != 8 && direction != 6 && direction != 2);

				if (direction == 4)
					game.moveLeft();

				if (direction == 8)
					game.moveUp();

				if (direction == 6)
					game.moveRight();

				if (direction == 2)
					game.moveDown();

				System.out.println(game);
			}
		} finally {
			if (scanner != null) {
				scanner.close();
			}
		}
	}
}
