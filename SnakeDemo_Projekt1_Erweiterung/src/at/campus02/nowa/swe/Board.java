package at.campus02.nowa.swe;

public class Board extends Gameobject
{
	public Board(int size)
	{
	super();
	_board = new Gameobject[size][size];
	
	
	for (int y = 0; y < _board.length; y++) {
		for (int x = 0; x < _board[0].length; x++) {
			if (y == 0 || y == _board.length -1) {
				_board[x][y] = 'x';
			}else if (x == 0 || x == _board.length -1) {
				_board[x][y] = 'x';
			}
		}
	}

	}
}
