package at.campus02.nowa.swe;

public abstract class Gameobject
{
	protected Gameobject[][] _board;
	
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int y = 0; y < _board.length; y++) {
			for (int x = 0; x < _board[0].length; x++) {
				sb.append(_board[x][y]);
			}
			sb.append("\n");
		}
		return sb.toString();
	}
}
