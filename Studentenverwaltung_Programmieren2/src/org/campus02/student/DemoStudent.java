package org.campus02.student;

import java.util.LinkedList;
import java.util.ListIterator;

public class DemoStudent
{

	public static void main(String[] args)
	{
		LinkedList<Studenten>students = new LinkedList<Studenten>();
		
		Studenten s1 = new Studenten("Max");
		Studenten s2 = new Studenten("Peter");
		Studenten s3 = new Studenten("John");
		Studenten s4 = new Studenten("Phil");
		
		students.addFirst(s1);
		students.addFirst(s2);
		students.addFirst(s3);
		students.addFirst(s4);

		ListIterator<Studenten> iterator = students.listIterator();
		
		// wenn ich es ausgeben m�chte dann muss ich eine variable anlegen 
//		Studenten st = iterator.next();
//		System.out.println(st);	
		
		// wenn ich nur System.out.println(iterator.next()); 
		//nach dem iterator.next() mach dann macht er mir das doppelt und gibt mir das nicht aus 
//		iterator.next();
//		System.out.println(iterator.next());
		
		iterator.next();
		
		iterator.next();
		
		iterator.add(new Studenten ("Marie"));
		iterator.add(new Studenten ("Nena"));
		
		iterator.next();
		
		iterator.remove();
		
		iterator = students.listIterator(); // weil ich Marie und Nena unten eingef�gt hab muss ich das noch einmal hinschreiben
											//wenn ich es oben hingeschrieben h�tte dann m�sste ich das nicht machen
		while(iterator.hasNext())
		{
			System.out.println(iterator.next());
		}
	}

}
