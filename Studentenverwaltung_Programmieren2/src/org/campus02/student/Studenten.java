package org.campus02.student;

public class Studenten
{
	private String name;

	public Studenten (String name)
	{
		this.name = name;
	}

	
	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String toString()
	{
		return name.toString();
	}
}
