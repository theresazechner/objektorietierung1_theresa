package edu.campus02.dosen;

public class Keksdose {
	
	private String farbe;
	private double x;
	private double y;
	private double hoehe;

	public Keksdose(String farbe, double x, double y, double hoehe) 
	{
		this.farbe = farbe;
		this.x = x;
		this.y = y;
		this.hoehe = hoehe;
	}

	public double getBreite() 
	{
		return x;
	}

	public double getLaenge() {
		return y;
	}

	public double getHoehe() {
		return hoehe;
	}

	public double volumen() {
		return 0;
	}

	public double freiesVolumen() {
		return 0;
	}

	public boolean keksAblegen(Keks keks) {

		return false;
	}

	public boolean leer() {
		return false;
	}

	public boolean stapeln(Keksdose d) {

		return false;
	}

	public double gesamtVolumen() {
		return 0;
	}
	public String toString()
	{
		return String.format("(%s %.1f %s %.1f %s %.1f)",farbe,x,"x",y,"x",hoehe);
	}
}
