package edu.campus02.fruechte;

import java.util.ArrayList;

public class ObstKorb {
	
	private ArrayList<Frucht> obst = new ArrayList<Frucht>();

	public void fruchtAblegen(Frucht f) 
	{
		obst.add(f);
	}

	public int zaehleFruechte(String sorte) 
	{
		ArrayList<Frucht> fruchtanzahl = new ArrayList<Frucht>();
		int result = 0;
		
		Frucht zaehle_fruechte = obst.get(0);
		
		for(int index = 0; index < obst.size(); index++)
		{
			zaehle_fruechte = obst.get(index);
			
			if(zaehle_fruechte.getSorte() == sorte)
			{
				fruchtanzahl.add(zaehle_fruechte);
			}
			result = fruchtanzahl.size();
		}
		
		return result;
	}

	public Frucht schwersteFrucht() 
	{
		Frucht schwerste_frucht = obst.get(0);
		
		for(int index = 0; index < obst.size(); index++)
		{
			schwerste_frucht = obst.get(index);
			if(schwerste_frucht.getGewicht() > obst.size())
			{
				obst.add(schwerste_frucht);
			}
		}
		

		return schwerste_frucht;
	}

	public ArrayList<Frucht> fruechteSchwererAls(double gewicht) 
	{
		ArrayList<Frucht> fruechte_schwerer_als = new ArrayList<Frucht>();
		
		Frucht schwer = obst.get(0);
		
		for(int index = 0; index < obst.size(); index++)
		{
			schwer = obst.get(index);
			if(schwer.getGewicht() > gewicht)
			{
				fruechte_schwerer_als.add(schwer);
			}
		}
		return fruechte_schwerer_als;
	}

	
	public int zaehleSorte() 
	{	
		ArrayList<String> verschiedene_sorten = new ArrayList<String>();
		
		Frucht zaehle_sorten = obst.get(0);
		
		for(int index = 0; index < obst.size(); index++)
		{
			zaehle_sorten = obst.get(index);
			
			if(!verschiedene_sorten.contains(zaehle_sorten.getSorte()))
				{
					verschiedene_sorten.add(zaehle_sorten.getSorte());
				}
		}		
		
		return verschiedene_sorten.size();
	}

	public String toString()
	{
		return String.format("%s", obst);
	}
}
