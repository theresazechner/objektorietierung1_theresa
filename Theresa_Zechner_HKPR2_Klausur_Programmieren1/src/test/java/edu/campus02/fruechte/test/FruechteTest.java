package edu.campus02.fruechte.test;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.campus02.fruechte.Frucht;

public class FruechteTest {

	
	@Test
	public void constructorTest() throws Exception {
		Frucht test = new Frucht("Melone", 300);
		
		assertEquals(300, test.getGewicht(),0.01);
		assertEquals("Melone", test.getSorte());
	}

	@Test
	public void toStringTest() throws Exception {
		Frucht test = new Frucht("Melone", 300);
		
		assertEquals("(Melone 300,00)", test.toString());
	}

}
