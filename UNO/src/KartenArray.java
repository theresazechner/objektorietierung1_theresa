import java.util.ArrayList;

import org.campus.UNO.Farbe;
import org.campus.UNO.Karte;

public class KartenArray
{

	public static void main(String[] args)
	{
		//int[] zahle = new int[] {1,2,3,4}; // Beispiel f�r ein Array
		
		Karte[] meineKarten = new Karte[] // da hab ich mein Array erstellt
				{ new Karte(Farbe.rot, 1), new Karte(Farbe.blau, 1), new Karte(Farbe.gelb, 10) };
		
		System.out.println(meineKarten[1]);
		
		for (Karte karte : meineKarten)
		{
			System.out.println(karte);
		}

		// Typ ArrayList die Karten verwalten kann
		ArrayList<Karte> meineListe = new ArrayList<Karte>(); // meineListe = Variable; Objekt = new
		
		// eine Karte in die Liste einf�gen
		meineListe.add(new Karte(Farbe.rot, 2));
		meineListe.add(new Karte(Farbe.gruen, 2));
		meineListe.add(new Karte(Farbe.blau, 3));
		
		//meineListe.add(0, new Karte(Farbe.gelb, 4)); // --> Karte ganz vorne dazu --> kann ich selber entscheiden
		
		
		// gibt mir die Karte an der Position 1
		System.out.println(meineListe.get(1));
		
		// eine Liste mit einer Schleife ausgeben
		for (Karte karte : meineListe) // foreach Schleife --> ich gebe Karten aus die sich in meiner Liste befinden
		{
			System.out.println(karte);
		}
		
		System.out.println(meineListe.size());
		
		System.out.println(meineListe.remove(0));
		
		System.out.println(meineListe.size());
		
		System.out.println(meineListe); // [gruen 2, blau 3] // wenn ich eine Liste hab dann erspar ich mir die toString Methode
		
		
	}

}
