

import org.campus.UNO.Farbe;
import org.campus.UNO.Karte;

public class KartenDemo
{

	public static void main(String [] args)
	{
		Karte karte1 = new Karte (Farbe.rot,4);
		Karte karte2 = new Karte (Farbe.gruen, 7);
		Karte karte3 = new Karte (Farbe.blau, 4);
		Karte karte4 = new Karte (Farbe.gelb, 3);
		Karte karte5 = new Karte (Farbe.blau, 1);
		Karte karte6 = new Karte (Farbe.rot, 5);
		Karte karte7 = new Karte (Farbe.gelb, 6);
		
		
		System.out.println(karte1);
		System.out.println(karte2);
		System.out.println(karte3);
		System.out.println(karte4);
		System.out.println(karte5);
		System.out.println(karte6);
		System.out.println(karte7);
		
		System.out.println(karte1.match(karte3));
		
		System.out.println(karte3.compare(karte1)); 
		System.out.println(karte1.compare(karte6)); 
		
	}
	
}
