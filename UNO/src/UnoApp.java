import org.campus.UNO.Spieler;
import org.campus.UNO.UNOSpiel;
import org.campus.UNO.Farbe;
import org.campus.UNO.Karte;

public class UnoApp
{

	public static void main(String[] args)
	{
		UNOSpiel meinSpiel = new UNOSpiel(); // mein Spiel ist das einzige was das UnoSpiel kennt
		
		Spieler sp1 = new Spieler("Maverick"); // Konstruktor ist das was hinter dem new kommt
		Spieler sp2 = new Spieler("Annabel");
		Spieler sp3 = new Spieler("Zane Cooper");
		
		
		meinSpiel.mitSpielen(sp1);
		meinSpiel.mitSpielen(sp2);
		meinSpiel.mitSpielen(sp3);
		
		meinSpiel.austeilen();
		
//		while(meinSpiel.spielzug()); // ich rufe die Methode auf weil ich alles im UNOSpiel schon definiert hab
									// da brauch ich ; weil ich eben alles in Methode definiert hab
		
		int z�hler = 1; // spielz�ge mitz�hlen	
		while(meinSpiel.spielzug())
		{
			System.out.print(z�hler + " ");
			z�hler++;
		}
		
//		System.out.println(sp1.passendeKarte(new Karte (Farbe.blau, 1)));
		
//		Karte abgehobeneKarte = meinSpiel.abheben();
//		System.out.println(abgehobeneKarte);
//		System.out.println(meinSpiel.abheben());
		

	}

}
