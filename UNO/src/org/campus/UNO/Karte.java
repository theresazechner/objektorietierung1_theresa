package org.campus.UNO;

public class Karte
{
	private Farbe farbe; // String durch Farbe von Enum austauschen 
	private int Zahl;
	
	public Karte(Farbe Kartenfarbe, int Kartenzahl) // Konstruktor --> immer der gleiche Name wie die Klasse
	{
		farbe = Kartenfarbe; // der Datentyp muss zuerst stehen sonnst kommt was falsches raus!!
		Zahl = Kartenzahl;
	}

	public boolean match(Karte ablegen) // bei booelan gibt es nur true oder false, Signatur einer Methode(Methodensignatur)
	{
		
		if(farbe == ablegen.farbe || Zahl == ablegen.Zahl)
		{
			return true;
		}else
		{
			return false;
		}
			
	}

	
		public boolean compare(Karte zweiteKarte)
		{	
			
			// ordinal gibt einen Zahlenwert mit der Stelle in Enum zur�ck
			if(farbe.ordinal() == zweiteKarte.farbe.ordinal())
			{
				boolean result = Zahl > zweiteKarte.Zahl; // return Zahl > zweiteKarte.Zahl; --> kurze Schreibweise
				return result;
			}else
			{
				// ordinal gibt einen Zahlenwert mit der Stelle in Enum zur�ck
				return farbe.ordinal() > zweiteKarte.farbe.ordinal();
			}
		}
		
	
		public String toString()
		{
			return farbe + " " +Zahl; // entweder farbe.toString() oder so wie ich es jetzt stehen hab
		}
}
