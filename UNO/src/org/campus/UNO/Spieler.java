package org.campus.UNO;

import java.util.ArrayList;

public class Spieler
{
	private String name; // ich brauche einen Spieler
	
	private ArrayList<Karte> handkarten = new ArrayList<Karte>(); // der Spieler braucht Handkarten
	
	public Spieler(String name) // Konstruktor = du mustt den Spieler einen namen geben
	{
		this.name = name;
	}

	public void aufnehmen(Karte karte)
	{
		handkarten.add(karte);
	}

	public String toString()
	{
		return name + " " + handkarten;
	}
	
	public Karte passendeKarte(Karte vergleichsKarte) // parameter --> ich merk es mir dort, mit passendeKarte gebe ich die vegleiskarte von au�en mit, die ich vergleichen m�chte
	{
		
		for (Karte karte : handkarten)
		{
			if(karte.match(vergleichsKarte))
			{
				handkarten.remove(karte);
				if(handkarten.size()==1)
				{
					System.out.println(" UNO ");
				}
				
				return karte;
			}
		}
		
		
		return null;
	}

	public int anzahlDerHandkarten()
	{
//		int result= handkarten.size(); --> ist das gleiche wie unten, soll es aber wie in unterer Variante schreiben
		return handkarten.size(); // handkarte ist Array List --> liefert zur�ck wie viele eintr�ge noch drinnen sind
								  // size liefert integer zur�ck
	
	}

}
