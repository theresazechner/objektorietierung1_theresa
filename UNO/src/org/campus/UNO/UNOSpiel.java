package org.campus.UNO;

import java.util.ArrayList;
import java.util.Collections;

public class UNOSpiel
{
	private ArrayList<Karte> ablageStapel = new ArrayList<Karte>();
	private ArrayList<Karte> kartenStapel = new ArrayList<Karte>();
	
	private ArrayList<Spieler> mitspieler = new ArrayList<Spieler>();
	
	public UNOSpiel()
	{
		
	  for (int index = 0; index < 2; index++) // damit ich 20 Karten bekomme --> schreibe ich es in eine zweite Scleife
		  									// so erspare ich mir das ich in der andern Schleife das alles noch einmsl schreibn muss
	  {
		for(int zahlenwert = 0; zahlenwert < 10; zahlenwert++)
		{
			kartenStapel.add(new Karte(Farbe.rot, zahlenwert)); // ich muss mir nicht immer eine neue Variable machen --> geht auch mit Liste das gemerkt wird an Methode weitergegeben wird
			kartenStapel.add(new Karte(Farbe.blau, zahlenwert));
			kartenStapel.add(new Karte(Farbe.gruen,zahlenwert));
			kartenStapel.add(new Karte(Farbe.gelb, zahlenwert));
			
		}
	  }
	
	  Collections.shuffle(kartenStapel); // Karten mischen; ist statisch = weil ich kein Objekt von der Klasse Collection brauche 
	  
	  
	}

	public Karte abheben() // liefert eine Karte zur�ck
	{
		
		// sind noch genug karten am Stapel?
		
		if(kartenStapel.size()==0)
		{
			// oberste karte merken
			Karte obersteKarte = ablageStapel.remove(0);
			
			Collections.shuffle(ablageStapel); // restlichen stapel mischen
			
			// aus ablagestapel neuen kartenstapel machen
			kartenStapel.addAll(ablageStapel); // alle Karten einf�gen
			
			ablageStapel.clear(); // kartenstapel leer und
			ablageStapel.add(obersteKarte); // oberste karte wieder drauf legen
		}
		
		
		return kartenStapel.remove(0); // ich nehme die Karte an der obersten stelle --> hebt immmer die Karte an stelle [0] ab
	}
	
	public void karteAblegen(Karte ablegeKarte) // bei Methodennamen kein leerzeichen im Namen
	{
		ablageStapel.add(0, ablegeKarte); // Karte an der Stelle 0 einf�gen
	}
	
	public void mitSpielen(Spieler neuerSpieler)
	{
		mitspieler.add(neuerSpieler);
	}

	public boolean spielzug()
	{
		// wer ist dran?
		Spieler aktuellerSpieler = mitspieler.remove(0); // wir nehmen ihn heraus --> er spielt un dann f�gen wir in (untere zeile) hinten wieder ein
		System.out.printf("am Zug ist: " + aktuellerSpieler);
		
		// spielerr vergleicht die oberste karte am ablagestapel mit den handkarten
		Karte gefundeneKarte = aktuellerSpieler.passendeKarte(ablageStapel.get(0));
		
		if(gefundeneKarte != null)
		{
			System.out.println(" Karte ablegen: " + gefundeneKarte);
			karteAblegen(gefundeneKarte);
		}else
		{
			System.out.println(" Karte ziehen ");
			Karte abgehobeneKarte = abheben();
			
			if(abgehobeneKarte.match(ablageStapel.get(0)))
			{
				System.out.println(" ablegen:" + abgehobeneKarte);
				karteAblegen(abgehobeneKarte);
			}
			else 
			{
				System.out.println(" Karte aufnehmen ");
				aktuellerSpieler.aufnehmen(abgehobeneKarte);
			}
		}
		
		if(aktuellerSpieler.anzahlDerHandkarten()==0)
		{
			System.out.println(" gewonnen hat: " + aktuellerSpieler);
			return false;
		}
		
		// spielzug ende spieler objekt als letztes in die liste einf�gen
		mitspieler.add(aktuellerSpieler);
		return true;
	}
	
	
	public void austeilen()
	{
	  for(int zaehler = 0; zaehler < 7; zaehler++)
	  {
		  
		// Jeden Mitspieler einmal "besuchen"
		for (Spieler sp : mitspieler)
		{
			Karte karte = abheben();
			sp.aufnehmen(karte);
		}
	  }
	
	  Karte temp = abheben();
	  ablageStapel.add(temp);
	  System.out.println(temp);
	}
	
 }

