
public class Zug
{
	private Wagon erster;
	private Wagon letzter;
	private int zuglaenge = 0; // es ist standardm��ig auf Null man muss nicht definieren
	
	public Zug()
	{
		// default Konstruktor --> leerer Konstruktor -->existiert auch ohne erstellen
	}

	public void neuerWagon(String inhalt)
	{
		Wagon neuerWagon = new Wagon(inhalt);
		
		// der Zug ist leer
		if(erster == null) // null --> die referenz zeigt noch ins leere
		{
			erster = neuerWagon;
			letzter = neuerWagon;
		} else // es gibt zumindest schon einen Wagon
		{
			letzter.einhaengen(neuerWagon);	// Methhode wird aufgerufen einhaengen, letzter = Wagon aus Wagon.java
			letzter = neuerWagon;
		}
		zuglaenge = zuglaenge +1; // --> zuglaenge++;
	}
	
	public String wagonInhaltAnStelle(int stelle)
	{
		if(stelle > zuglaenge -1 || stelle < 0) // || = oder --> das definiere ich damit keine Fehlermeldung kommt
												// wenn negative Zahl oder gr��er als Zugl�nge --> NullPointerExeption
		{
			return "Stelle nicht vorhanden!";
		}
		
		Wagon temp = erster;
		for(int springen=0; springen < stelle; springen++)
		{
			temp = temp.nachbar(); // zum n�chsten springen, temp= temp + 1 geht nicht weil ich nur f�r die ersten und den letzten eine Variable hab
		}
		
		return temp.toString();
	}
	
	public int lenght()
	{
		return zuglaenge;
	}
	
	public String toString()
	{
		if(erster == null)
		{
			return "";
		}else
		{
			String ergebnis = zuglaenge + ":";
			
			Wagon temp = erster;
			
			while(temp != null)
			{
				ergebnis = ergebnis + " " + temp.toString();
				temp = temp.nachbar();
			}
			
			return ergebnis;
		}
	
	}
}
