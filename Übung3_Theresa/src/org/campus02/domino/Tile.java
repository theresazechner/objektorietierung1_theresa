package org.campus02.domino;

public class Tile {
	
	private int h;
	private int s;

	public Tile(int h, int s) 
	{
		this.h = h;
		this.s = s;
		
	}

	public int getNumber1() 
	{
		if(h > s)
		{
			return h;
		}else
		{
			return s;
		}
		
	}

	public int getNumber2() 
	{
		if(h < s)
		{
			return h;
		}else
		{
			return s;
		}
	}

	public boolean compare(Tile otherTile) 
	{
//		if(getNumber1() < otherTile.getNumber1())
//		{
//			return true;
//		}
//		else if(getNumber1() == otherTile.getNumber1())
//		{
//			if(getNumber2() < otherTile.getNumber2())
//			{
//				return true;
//			}
//			else
//			{
//				return false;
//			}
//		}
//		else
//		{
//			return false;
//		}
//		
		boolean ergebnis = false;
		
		if(getNumber1() < otherTile.getNumber1())
		{
			ergebnis = true;
		}
		else if(getNumber1() == otherTile.getNumber1())
		{
			if(getNumber2() < otherTile.getNumber2())
			{
				ergebnis = true;
			}
		}
		return ergebnis;
	}

	@Override
	public String toString() 
	{
//		return "(" + h + "," + s + ")"; --> da gleiche wie oben
		
		return String.format("(%s,%s)",h,s);
		
	}
}
