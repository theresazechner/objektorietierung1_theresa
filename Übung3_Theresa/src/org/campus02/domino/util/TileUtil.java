package org.campus02.domino.util;

import java.util.ArrayList;

import org.campus02.domino.Tile;

public class TileUtil {

	public static int sum(ArrayList<Tile> tiles) 
	{
		int result = 0;
		
		Tile aktueller_stein = tiles.get(0);
		
		for(int index = 0; index < tiles.size(); index++)
		{
			aktueller_stein = tiles.get(index);
			
			result = result + aktueller_stein.getNumber1() + aktueller_stein.getNumber2();
		}
		
		return result;
	}

	public static Tile greatesTile(ArrayList<Tile> tiles) 
	{
			Tile result = tiles.get(0);
			
			Tile vergleichs_tile = tiles.get(1);
			
			for(int index = 1; index < tiles.size(); index++)
			{
				vergleichs_tile = tiles.get(index);
				
				if(result.compare(vergleichs_tile))
				{
					result = vergleichs_tile;
				}
			}
			
			return result;

	}
}
