package org.campus02.set;

import java.util.ArrayList;

public class Set {
	
	private ArrayList<Integer> wert = new ArrayList<Integer>();
	private ArrayList<Set> setter = new ArrayList<Set>();

	public void add(int value) 
	{
		wert.add(value);
	}
	
	// getterfunktion
//	public int getWert()
//	{
//		return wert;
//	}

	public void add(Set set) 
	{
		
		setter.add(set);
	}

	public int sum()
	{
		int summe = 0;
		
		for(int index = 0; index<wert.size(); index++)
		{
			summe = summe + wert.get(index);
		}
		
		return summe;
	}

	public String toString()
	{
		String result = "{";
		
		for(int index =0; index<wert.size(); index++)
		{
			result = result + wert.get(index);
			if(index<wert.size()-1)
			{
				result = result + ",";
			}
		}
		result = result + "}";
		return result;//String.format("{%s}", wert);
		
		//Versuch
//		return String.format("{%s,%s}",wert.toString(),setter.toString());
		
	}
}
