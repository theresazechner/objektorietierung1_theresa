package at.campus02.ase.filesystem;

import java.util.ArrayList;

public class Filesystem
{
//	private File b;
	private ArrayList<File> filesystem = new ArrayList<File>();
	

	public void addFile(File b)
	{
		filesystem.add(b);	// hinzufügen zu File b --> bei ArrayLists	
	}

	public int countSize()
	{
		int result = 0;
		
		for(int index = 0; index < filesystem.size(); index++) 
		{
			File datei = filesystem.get(index);
//			int groesse = datei.getSize(); --> kann ich auch direkt reinscheiben bei result
			
			result = result + datei.getSize();

		}
		return result;
	}

	public double averageSize()
	{
		double result = 0;
		double gesamtgroesse = countSize();
	
		result =  gesamtgroesse /filesystem.size();
		return result;

	}

	public ArrayList<File> fileByOwner(String owner)
	{
		ArrayList<File> files_from_owner = new ArrayList<File>(); // ich muss neue ArrayList erstellen
		
		if(owner == null)
		{
			return files_from_owner;
		}
		
		File file_nach_autor_durchsuchen = filesystem.get(0); // File wird erstellt --> statt index kann ich null hineinschreiben --> kommt in diesem Fall aufs gleiche
		
		for(int index = 0; index < filesystem.size(); index++)
		{
			file_nach_autor_durchsuchen = filesystem.get(index); // wird auf file_nach_autor_durchsuchen geschrieben

			// wenn eine Übereinstimmung zwischen dem gesuchten Autor und des Files vorhanden ist....
			if(file_nach_autor_durchsuchen.getOwner() == owner) // diese Aussage ist entweder wahr oder falsch --> immer so!
			{
				
				files_from_owner.add(file_nach_autor_durchsuchen); // dann speichere ich das Ergebnis in meiner oben erstellten ArrayList
			
			}
			
		}

			return files_from_owner;
			
	}

	
	public ArrayList<File> findFile(String search)
	{

		return null;
	}
}
