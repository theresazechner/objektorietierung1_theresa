package at.campus02.ase.messages;

import java.util.ArrayList;

public class Message
{
	private String titel;
	private String text;
	
	private ArrayList<Message> antworten = new ArrayList<Message>();

	public Message(String titel, String text)
	{
		this.titel = titel;
		this.text = text;
	}

	public Message antworten(String titel, String text)
	{
		Message neu = new Message(titel, text);
		antworten.add(neu);
		
//		antworten.add(new Message(titel,text)); --> ist das gleiche wie oben nur tu ich mir so schwerer beim returnen
		
//		return new Message(titel,text);
		
		return neu;
	}

	public ArrayList<Message> getAntworten()
	{
		return antworten;
	}

	public int countMessages()
	{
		int anzahl_eintraege = 0;
		for(int index = 0; index < antworten.size(); index++)
		{
			anzahl_eintraege = anzahl_eintraege + antworten.size() + 1; //--> hab es mit + 1 geschafft aber warum? weil ich die Hauptmessage auch dazuz�hlen muss? 
		}
		
		return anzahl_eintraege;
	}

	public String toString()
	{
//		return "(" + titel + "," + text + ")"; --> von der ersten Aufgabe --> wird durch erweiterte toString Methode ersetzt
		String sammlung_antworten = "(" + titel + "," + text + ")"; 
		Message zwischenergebnis = antworten.get(0); 
//		ArrayList<Message> antworten_auf_zwischenergebnis = antworten.get(0);
	
		
		if(antworten.size() > 1) // er darf es nur machen wenn es genau 1 ist!!
		{
			sammlung_antworten = sammlung_antworten + "[";
		}
		for(int index = 0; index < antworten.size(); index++)
		{
			zwischenergebnis = antworten.get(index);
//			antworten_auf_zwischenergebnis = zwischenergebnis.getAntworten();
//			zwischenergebnis.countMessages();
//			zwischenergebnis.getAntworten();

			sammlung_antworten = sammlung_antworten + "(" + zwischenergebnis.titel + "," + zwischenergebnis.text + ")";
			
		}
		if(antworten.size() > 1)
		{
			sammlung_antworten = sammlung_antworten + "]";
		}
		return sammlung_antworten; 
	}

	
}
