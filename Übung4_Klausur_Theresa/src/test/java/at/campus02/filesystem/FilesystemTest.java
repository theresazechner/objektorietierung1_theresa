package at.campus02.filesystem;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import at.campus02.ase.filesystem.File;
import at.campus02.ase.filesystem.Filesystem;

public class FilesystemTest {
	@Test
	public void konstruktor() throws Exception {

		File b = new File("Fliess", "test.pdf", 400);

		assertEquals("[Fliess,test.pdf,400]", b.toString());
	}

	@Test
	public void size() throws Exception {

		Filesystem fs = new Filesystem();

		fs.addFile(new File("owner1", "1.txt", 200));
		fs.addFile(new File("owner1", "2.txt", 100));
		fs.addFile(new File("owner2", "3.txt", 400));
		fs.addFile(new File("owner1", "4.txt", 50));
		fs.addFile(new File("owner3", "5.txt", 50));

		assertEquals(800, fs.countSize());
	}

	@Test
	public void durchschnitt() throws Exception {

		Filesystem bibliothek = new Filesystem();

		bibliothek.addFile(new File("owner1", "1", 200));
		bibliothek.addFile(new File("owner1", "2", 100));
		bibliothek.addFile(new File("owner2", "3", 400));
		bibliothek.addFile(new File("owner1", "4", 50));
		bibliothek.addFile(new File("owner3", "5", 57));

		assertEquals(161.4, bibliothek.averageSize(), 0.001);
	}

	@Test
	public void owner01() throws Exception {

		Filesystem bibliothek = new Filesystem();

		bibliothek.addFile(new File("owner1", "1", 200));
		bibliothek.addFile(new File("owner1", "2", 100));
		bibliothek.addFile(new File("owner2", "3", 400));
		bibliothek.addFile(new File("owner1", "4", 50));
		bibliothek.addFile(new File("owner3", "5", 57));

		ArrayList<File> booksByowner = bibliothek.fileByOwner("owner1");
		assertEquals(3, booksByowner.size());
	}
	@Test
	public void owner02() throws Exception {

		Filesystem bibliothek = new Filesystem();

		bibliothek.addFile(new File("owner1", "1", 200));
		bibliothek.addFile(new File("owner1", "2", 100));
		bibliothek.addFile(new File("owner2", "3", 400));
		bibliothek.addFile(new File("owner1", "4", 50));
		bibliothek.addFile(new File("owner3", "5", 57));

		ArrayList<File> booksByowner = bibliothek.fileByOwner(null);
		assertEquals(0, booksByowner.size());
	}

	@Test
	public void find01() throws Exception {

		Filesystem bibliothek = new Filesystem();

		bibliothek.addFile(new File("owner1", "1", 200));
		bibliothek.addFile(new File("owner1", "2", 100));
		bibliothek.addFile(new File("owner2", "3", 400));
		bibliothek.addFile(new File("owner1", "4", 50));
		bibliothek.addFile(new File("owner3", "5", 57));

		ArrayList<File> find = bibliothek.findFile("3");
		assertEquals(2, find.size());
	}

	@Test
	public void find02() throws Exception {

		Filesystem bibliothek = new Filesystem();

		bibliothek.addFile(new File("owner1", "1", 200));
		bibliothek.addFile(new File("owner1", "2", 100));
		bibliothek.addFile(new File("owner2", "3", 400));
		bibliothek.addFile(new File("owner1", "4", 50));
		bibliothek.addFile(new File("owner3", "5", 57));

		ArrayList<File> find = bibliothek.findFile(null);
		assertEquals(0, find.size());
	}
}
